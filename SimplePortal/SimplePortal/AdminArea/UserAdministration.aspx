﻿<%@ Page Title="User Administration" Language="C#" MasterPageFile="~/MasterPages/Portal.Master" AutoEventWireup="true" CodeBehind="UserAdministration.aspx.cs" Inherits="SimplePortal.AdminArea.UserAdministration" %>

<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    User Administration
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
<div class="col-md-4 column1">
    <div class="ContentContainer">
        <br />
        <h3>Add/ Update accessGroup</h3>
        <br />
        <div class="LargeFont">
        Username:
        </div>
        <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
        <br />
        <br />
        <div class="LargeFont">
        Access Group:</div>
        <asp:TextBox ID="txtAccessGroup" runat="server" ></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnEditUserGroup" runat="server" OnClick="btnEditUserGroup_Click" Text="Edit Access Group" CssClass="button"/>
        <br />
        <br />
        <asp:Label ID="lblUpdateResponse" runat="server"></asp:Label>
        <br />
        <br />

<%--
            <p>
        Select User:
        <asp:Label ID="lblSelectUser" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
       <asp:DropDownList ID="ddlSelectUser" runat="server" OnSelectedIndexChanged="ddlSelectUser_SelectedIndexChanged" AutoPostBack="True" DataTextField="title" datakeynames="id">
            <asp:ListItem Value="Select">Select User</asp:ListItem>
            <asp:ListItem Value="david@intraspire.com">David Page</asp:ListItem>
            <asp:ListItem Value="Second">Second FBO</asp:ListItem>
            <asp:ListItem Value="Third">Third</asp:ListItem>
        </asp:DropDownList>
    </p>
    

        <div class ="SmallFont">
    <p>
        Assign Access Group:
        <asp:Label ID="lblAccessGroup" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:DropDownList ID="ddlSelectAccessGroup" runat="server" OnSelectedIndexChanged="ddlSelectAccessGroup_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Value="Select">Select Access Group</asp:ListItem>
            <asp:ListItem Value="flytzeAdmin">FlytzeAdmin</asp:ListItem>
            <asp:ListItem Value="winner">Winner</asp:ListItem>
            <asp:ListItem Value="fbo2">FBO2</asp:ListItem>
        </asp:DropDownList>
    </p>    
</div>
    --%>

</div>
            </div>

<div class="col-md-3 col-lg-4 column2 ">
    <div class="ContentContainer">
<h3>Create User</h3>
        All required attribures in Account colection 
        <p>
            &nbsp;</p>
            <table id="TableAddAccount">
                <tr class="TableTitles">
                    <td>Attribute</td>
                    <td class="auto-style2">Entry Value</td>
                </tr>      
                <tr>
                    <td>Username:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Password:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox2" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>First Name:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox3" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Last Name:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox4" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>&nbsp;Phone #:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox5" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                
            </table>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Create New User" CssClass="button" OnClick="CreateUser_Click"/>
        <br />
</div> 
</div>


<div class="col-md-3 col-lg-4 column2 ">
<div class="ContentContainer">

        <h3>View and Maintain Users  </h3>
        <p>
        edit user details<br />
    </p>
    <p>
        Ideally this would be a &quot;getAllAccounts&quot; function that populates a Grid View that supports edit rows function</p>
    <p>
        <%--<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
        </asp:GridView>
    </p>
    <p>
        &nbsp;</p>
    <p>
        If not, then we&#39;ll use drop down list of all users to select one to edit.</p>
    <p>
        View and Edit selected user:</p>
    <p>
        call getAccount function:</p>
    <p>
        <asp:DropDownList ID="DropDownList1" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        [Edit info in table similar to MOMaintenance page.]--%></p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</div>
</div>
</asp:Content>
