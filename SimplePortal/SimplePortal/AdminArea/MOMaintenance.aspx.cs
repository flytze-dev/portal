﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Objects;
using SimplePortal.Results;
using SimplePortal.Helpers;
using Newtonsoft.Json;

namespace SimplePortal.AdminArea
{
    public partial class MOMaintenance : System.Web.UI.Page
    {
        editMO currentDefaultMO = new editMO();
        WinnerCosts currentDefaultMOList = new WinnerCosts();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoggedInUserAccessGroup"] != null && Session["LoggedInUser"] != null)
            { 
            var loggedInUserToken = Session["LoggedInUserToken"];
            var loggedInUser = Session["LoggedInUser"];
            var loggedInUserAccessGroup = Session["LoggedInUserAccessGroup"];



            if (!IsPostBack)
            {
                #region "Initiate Default MO"
                //var viewMO = new RequestByUsername();
                //viewMO.username = "defaultMO@MO.com";
                //var moResponse = RestClientHelper.Post<editMO>("viewMo", viewMO, "000AD99nV6BeXvoE3DfFsX000", "defaultMO@MO.com");
                ////currentDefaultMO = JsonConvert.DeserializeObject<editMO>(moResponse.FunctionResponse.ToString());


                var fboToDisplay = new RequestById();
                //fboToDisplay.id = "56f42b695e38a90f42f2e7c2";
                //used for Quote
                fboToDisplay.id = "55a74297e4b07e03cc6ac65e";

                var fboResponseforFactor = RestClientHelper.Post<WinnerCosts>("readFbo", fboToDisplay, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
                currentDefaultMOList = JsonConvert.DeserializeObject<WinnerCosts>(fboResponseforFactor.FunctionResponse.ToString());

                //btnTestDefaultMO.Visible = false;


                #region "Want to Loop"
                //Tried to run loop but have to do it 1 by 1.
                //for (int i = 0; i <= 10; i++)
                //{
                //    if (!String.IsNullOrEmpty(currentDefaultMOList.fees.factors[i].ToString()))
                //    {
                //        //this.Page.FindControl("lblAllpax1").Visible = false;
                //        var pax = i + 1;
                //        this.Controls["lblAllpax" + pax.ToString()].Visible = false;                       )
                //    }        
                //}


                //if (!String.IsNullOrEmpty(currentDefaultMOList.fees.factors[i].ToString()))
                //{
                //    newOperatorMO.pax1 = Convert.ToDouble(txtOpPax1.Text);
                //    lblOpPax1.Text = newOperatorMO.pax2.ToString("P");
                //}

                //lblAllpax1.Text = currentDefaultMO.pax1.ToString("P");
                //lblAllpax2.Text = currentDefaultMO.pax2.ToString("P");
                //lblAllpax3.Text = currentDefaultMO.pax3.ToString("P");
                //lblAllpax4.Text = currentDefaultMO.pax4.ToString("P");
                //lblAllpax5.Text = currentDefaultMO.pax5.ToString("P");
                //lblAllpax6.Text = currentDefaultMO.pax6.ToString("P");
                //lblAllpax7.Text = currentDefaultMO.pax7.ToString("P");
                //lblAllpax8.Text = currentDefaultMO.pax8.ToString("P");
                //lblAllpax9.Text = currentDefaultMO.pax9.ToString("P");
                //lblAllpax10.Text = currentDefaultMO.pax10.ToString("P");
                //txtAllPax1.Text = currentDefaultMO.pax1.ToString();
                //txtAllPax2.Text = currentDefaultMO.pax2.ToString();
                //txtAllPax3.Text = currentDefaultMO.pax3.ToString();
                //txtAllPax4.Text = currentDefaultMO.pax4.ToString();
                //txtAllPax5.Text = currentDefaultMO.pax5.ToString();
                //txtAllPax6.Text = currentDefaultMO.pax6.ToString();
                //txtAllPax7.Text = currentDefaultMO.pax7.ToString();
                //txtAllPax8.Text = currentDefaultMO.pax8.ToString();
                //txtAllPax9.Text = currentDefaultMO.pax9.ToString();
                //txtAllPax10.Text = currentDefaultMO.pax10.ToString();
                #endregion

                //lblAllpax1.Text = currentDefaultMOList.fees.factors[0].ToString("P");
                lblAllpax2.Text = currentDefaultMOList.fees.factors[1].ToString("P");
                lblAllpax3.Text = currentDefaultMOList.fees.factors[2].ToString("P");
                lblAllpax4.Text = currentDefaultMOList.fees.factors[3].ToString("P");
                lblAllpax5.Text = currentDefaultMOList.fees.factors[4].ToString("P");
                lblAllpax6.Text = currentDefaultMOList.fees.factors[5].ToString("P");
                lblAllpax7.Text = currentDefaultMOList.fees.factors[6].ToString("P");
                lblAllpax8.Text = currentDefaultMOList.fees.factors[7].ToString("P");
                lblAllpax9.Text = currentDefaultMOList.fees.factors[8].ToString("P");
                lblAllpax10.Text = currentDefaultMOList.fees.factors[9].ToString("P");
                //txtAllPax1.Text = currentDefaultMOList.fees.factors[0].ToString();
                txtAllPax2.Text = currentDefaultMOList.fees.factors[1].ToString();
                txtAllPax3.Text = currentDefaultMOList.fees.factors[2].ToString();
                txtAllPax4.Text = currentDefaultMOList.fees.factors[3].ToString();
                txtAllPax5.Text = currentDefaultMOList.fees.factors[4].ToString();
                txtAllPax6.Text = currentDefaultMOList.fees.factors[5].ToString();
                txtAllPax7.Text = currentDefaultMOList.fees.factors[6].ToString();
                txtAllPax8.Text = currentDefaultMOList.fees.factors[7].ToString();
                txtAllPax9.Text = currentDefaultMOList.fees.factors[8].ToString();
                txtAllPax10.Text = currentDefaultMOList.fees.factors[9].ToString();

                Session["DefaultMO"] = currentDefaultMOList;

                //txtAllPax1.Visible = false;
                txtAllPax2.Visible = false;
                txtAllPax3.Visible = false;
                txtAllPax4.Visible = false;
                txtAllPax5.Visible = false;
                txtAllPax6.Visible = false;
                txtAllPax7.Visible = false;
                txtAllPax8.Visible = false;
                txtAllPax9.Visible = false;
                txtAllPax10.Visible = false;
                #endregion

                #region "Initiate Operator"
                TableEditFBO.Visible = false;
                btnEditOperator.Visible = false;
                btnEditPlane.Visible = false;

                //FBOList list = new FBOList();
                //list.name = "Winner";
                //list.id = "12345";

                //DropDownList list2 = new DropDownList();
                //list2.ID = "list2";
                //list2.Items.Add(new ListItem("Winner", "1"));
                //list2.Items.Add(new ListItem("Second FBO", "2"));
                //list2.Items.Add(new ListItem("Third", "3"));

                //list.DataSource = GetListItems(); // <-- Get your data from somewhere.
                //list.DataValueField = "ValueProperty";
                //list.DataTextField = "TextProperty";
                //list.DataBind();

                //ddlOperatorToEdit.DataSource = formData;
                //ddlOperatorToEdit.DataBind();

                //not using requestByUsername
                //var viewOperatorMO = new RequestByUsername();
                //viewOperatorMO.username = "operatorMO@MO.com";
                //var moOperatorResponse = RestClientHelper.Post<editMO>("viewMo", viewOperatorMO, "022AD99nV6BeXvoE3DfFsX000", "operatorMO@MO.com");
                //var currentOperatorMO = JsonConvert.DeserializeObject<editMO>(moOperatorResponse.FunctionResponse.ToString());

                //Session["OperatorMO"] = currentDefaultMO;

//////why duplicate call?
                //var operatorToDisplay = new RequestById();
                ////fboToDisplay.id = "56f42b695e38a90f42f2e7c2";
                ////used for Quote
                //operatorToDisplay.id = "55a74297e4b07e03cc6ac65e";

                //var operatorResponseforFactor = RestClientHelper.Post<WinnerCosts>("readFbo", operatorToDisplay, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
                //var success = JsonConvert.DeserializeObject<WinnerCosts>(operatorResponseforFactor.FunctionResponse.ToString());


                //var emptyToReadFBOs = new Object();
                //var moOperatorsResponses = RestClientHelper.Post<List<WinnerCosts>>("readFbos", emptyToReadFBOs, "022AD99nV6BeXvoE3DfFsX000", "operatorMO@MO.com");
                //var currentOperatorsMOs = JsonConvert.DeserializeObject<List<WinnerCosts>>(moOperatorsResponses.FunctionResponse.ToString());

                //Session["OperatorsMOs"] = currentOperatorsMOs;






                ddlOperatorToEdit.SelectedIndex = -1;
                TableEditFBO.Visible = false;
                btnEditOperator.Visible = false;

                #endregion

                #region "Initiate Plane"
                var viewPlaneMO = new RequestById();
                viewPlaneMO.id = "55a74198e4b07e03cc6ac65a";
                var moPlaneResponse = RestClientHelper.Post<Aircraft>("readPlane", viewPlaneMO, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
                var currentPlaneMO = JsonConvert.DeserializeObject<Aircraft>(moPlaneResponse.FunctionResponse.ToString());

                Session["CurrentPlaneMO"] = currentPlaneMO;
                //Session["CurrentPlaneMO"] = currentPlaneMO.fees.factors;

                TableEditSpecificPlane.Visible = false;
                ddlSelectPlane.Visible = false;
                ddlSelectPlane.SelectedIndex = -1;
                ddlPlaneOperator.SelectedIndex = -1;
                btnEditPlane.Visible = false;

                //lblPlanePax1.Text = currentPlaneMO.fees.factors[0].ToString("P");
                lblPlanePax2.Text = currentPlaneMO.fees.factors[1].ToString("P");
                lblPlanePax3.Text = currentPlaneMO.fees.factors[2].ToString("P");
                lblPlanePax4.Text = currentPlaneMO.fees.factors[3].ToString("P");
                lblPlanePax5.Text = currentPlaneMO.fees.factors[4].ToString("P");
                lblPlanePax6.Text = currentPlaneMO.fees.factors[5].ToString("P");
                lblPlanePax7.Text = currentPlaneMO.fees.factors[6].ToString("P");
                lblPlanePax8.Text = currentPlaneMO.fees.factors[7].ToString("P");
                lblPlanePax9.Text = currentPlaneMO.fees.factors[8].ToString("P");
                lblPlanePax10.Text = currentPlaneMO.fees.factors[9].ToString("P");
                //txtPlanePax1.Text = currentPlaneMO.fees.factors[0].ToString();
                txtPlanePax2.Text = currentPlaneMO.fees.factors[1].ToString();
                txtPlanePax3.Text = currentPlaneMO.fees.factors[2].ToString();
                txtPlanePax4.Text = currentPlaneMO.fees.factors[3].ToString();
                txtPlanePax5.Text = currentPlaneMO.fees.factors[4].ToString();
                txtPlanePax6.Text = currentPlaneMO.fees.factors[5].ToString();
                txtPlanePax7.Text = currentPlaneMO.fees.factors[6].ToString();
                txtPlanePax8.Text = currentPlaneMO.fees.factors[7].ToString();
                txtPlanePax9.Text = currentPlaneMO.fees.factors[8].ToString();
                txtPlanePax10.Text = currentPlaneMO.fees.factors[9].ToString();
                //txtPlanePax1.Visible = true;
                txtPlanePax2.Visible = true;
                txtPlanePax3.Visible = true;
                txtPlanePax4.Visible = true;
                txtPlanePax5.Visible = true;
                txtPlanePax6.Visible = true;
                txtPlanePax7.Visible = true;
                txtPlanePax8.Visible = true;
                txtPlanePax9.Visible = true;
                txtPlanePax10.Visible = true;
                //lblPlanePax1.Visible = false;
                lblPlanePax2.Visible = false;
                lblPlanePax3.Visible = false;
                lblPlanePax4.Visible = false;
                lblPlanePax5.Visible = false;
                lblPlanePax6.Visible = false;
                lblPlanePax7.Visible = false;
                lblPlanePax8.Visible = false;
                lblPlanePax9.Visible = false;
                lblPlanePax10.Visible = false;
                #endregion
            }
        }
        }
        #region "Default MO Area"
        protected void btnEditDefaultMO_Click(object sender, EventArgs e)
        {
            //txtAllPax1.Visible = true;
            txtAllPax2.Visible = true;
            txtAllPax3.Visible = true;
            txtAllPax4.Visible = true;
            txtAllPax5.Visible = true;
            txtAllPax6.Visible = true;
            txtAllPax7.Visible = true;
            txtAllPax8.Visible = true;
            txtAllPax9.Visible = true;
            txtAllPax10.Visible = true;

            //lblAllpax1.Visible = false;
            lblAllpax2.Visible = false;
            lblAllpax3.Visible = false;
            lblAllpax4.Visible = false;
            lblAllpax5.Visible = false;
            lblAllpax6.Visible = false;
            lblAllpax7.Visible = false;
            lblAllpax8.Visible = false;
            lblAllpax9.Visible = false;
            lblAllpax10.Visible = false;

            btnEditDefaultMO.Visible = false;
            btnTestDefaultMO.Visible = true;

            btnEditOperator.Visible = false;
            btnConfirmOperator.Visible = false;
            ddlOperatorToEdit.Visible = false;

            btnEditPlane.Visible = false;
            btnConfirmPlane.Visible = false;
            ddlPlaneOperator.Visible = false;
        }

         protected void btnTestDefaultMO_Click(object sender, EventArgs e)
        {
            WinnerCosts newDefaultMO = (WinnerCosts)Session["DefaultMO"];

            //if (!String.IsNullOrEmpty(txtAllPax1.Text))
            //{
            //    newDefaultMO.fees.factors[0] = Convert.ToDouble(txtAllPax1.Text);
            //    //newDefaultMO.pax1 = Convert.ToDouble(txtAllPax1.Text);
            //    //lblAllpax1.Text = newDefaultMO.pax2.ToString("P");
            //    lblAllpax1.Text = newDefaultMO.fees.factors[0].ToString("P");
            //}
            if (!String.IsNullOrEmpty(txtAllPax2.Text))
            {
                //newDefaultMO.pax2 = Convert.ToDouble(txtAllPax2.Text);
                //lblAllpax2.Text = newDefaultMO.pax2.ToString("P");
                newDefaultMO.fees.factors[1] = Convert.ToDouble(txtAllPax2.Text);
                lblAllpax2.Text = newDefaultMO.fees.factors[1].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtAllPax3.Text))
            {
                //newDefaultMO.pax3 = Convert.ToDouble(txtAllPax3.Text);
                //lblAllpax3.Text = newDefaultMO.pax3.ToString("P");
                newDefaultMO.fees.factors[2] = Convert.ToDouble(txtAllPax3.Text);
                lblAllpax3.Text = newDefaultMO.fees.factors[2].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtAllPax4.Text))
            {
                //newDefaultMO.pax4 = Convert.ToDouble(txtAllPax4.Text);
                //lblAllpax4.Text = newDefaultMO.pax4.ToString("P");
                newDefaultMO.fees.factors[3] = Convert.ToDouble(txtAllPax4.Text);
                lblAllpax4.Text = newDefaultMO.fees.factors[3].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtAllPax5.Text))
            {
                //newDefaultMO.pax5 = Convert.ToDouble(txtAllPax5.Text);
                //lblAllpax5.Text = newDefaultMO.pax5.ToString("P");
                newDefaultMO.fees.factors[4] = Convert.ToDouble(txtAllPax5.Text);
                lblAllpax5.Text = newDefaultMO.fees.factors[4].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtAllPax6.Text))
            {
                //newDefaultMO.pax6 = Convert.ToDouble(txtAllPax6.Text);
                //lblAllpax6.Text = newDefaultMO.pax6.ToString("P");
                newDefaultMO.fees.factors[5] = Convert.ToDouble(txtAllPax6.Text);
                lblAllpax6.Text = newDefaultMO.fees.factors[5].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtAllPax7.Text))
            {
                //newDefaultMO.pax7 = Convert.ToDouble(txtAllPax7.Text);
                //lblAllpax7.Text = newDefaultMO.pax7.ToString("P");
                newDefaultMO.fees.factors[6] = Convert.ToDouble(txtAllPax7.Text);
                lblAllpax7.Text = newDefaultMO.fees.factors[6].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtAllPax8.Text))
            {
                //newDefaultMO.pax8 = Convert.ToDouble(txtAllPax8.Text);
                //lblAllpax8.Text = newDefaultMO.pax8.ToString("P");
                newDefaultMO.fees.factors[7] = Convert.ToDouble(txtAllPax8.Text);
                lblAllpax8.Text = newDefaultMO.fees.factors[7].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtAllPax9.Text))
            {
                //newDefaultMO.pax9 = Convert.ToDouble(txtAllPax9.Text);
                //lblAllpax9.Text = newDefaultMO.pax9.ToString("P");
                newDefaultMO.fees.factors[8] = Convert.ToDouble(txtAllPax9.Text);
                lblAllpax9.Text = newDefaultMO.fees.factors[8].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtAllPax10.Text))
            {
                //newDefaultMO.pax10 = Convert.ToDouble(txtAllPax10.Text);
                //lblAllpax10.Text = newDefaultMO.pax10.ToString("P");
                newDefaultMO.fees.factors[9] = Convert.ToDouble(txtAllPax10.Text);
                lblAllpax10.Text = newDefaultMO.fees.factors[9].ToString("P");
            }

            //newDefaultMO.username = "defaultMO@MO.com";
            //var success = JsonConvert.DeserializeObject<editMO>(moResponse.FunctionResponse.ToString());

            sendUpdate fboUpdateToSend = new sendUpdate();
            fboUpdateToSend.id = "55a74297e4b07e03cc6ac65e";                               
            fboUpdateToSend.updates = newDefaultMO;

            var fboUpdateResponse = RestClientHelper.Post<editMO>("updateFbo", fboUpdateToSend, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
            //  var isSucess = fboUpdateResponse.Success.ToString();
            var success = JsonConvert.DeserializeObject<WinnerCosts>(fboUpdateResponse.FunctionResponse.ToString());


            //txtAllPax1.Visible = false;
            txtAllPax2.Visible = false;
            txtAllPax3.Visible = false;
            txtAllPax4.Visible = false;
            txtAllPax5.Visible = false;
            txtAllPax6.Visible = false;
            txtAllPax7.Visible = false;
            txtAllPax8.Visible = false;
            txtAllPax9.Visible = false;
            txtAllPax10.Visible = false;
            //lblAllpax1.Visible = true;
            lblAllpax2.Visible = true;
            lblAllpax3.Visible = true;
            lblAllpax4.Visible = true;
            lblAllpax5.Visible = true;
            lblAllpax6.Visible = true;
            lblAllpax7.Visible = true;
            lblAllpax8.Visible = true;
            lblAllpax9.Visible = true;
            lblAllpax10.Visible = true;
            
            btnEditDefaultMO.Visible = true;
            btnTestDefaultMO.Visible = false;

            btnEditOperator.Visible = false;
            btnConfirmOperator.Visible = false;
            ddlOperatorToEdit.Visible = true;

            btnEditPlane.Visible = false;
            btnConfirmPlane.Visible = false;
            ddlPlaneOperator.Visible = true;
        }
        #endregion

        #region "Operator MO Area"
        protected void ddlOperatorToEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            TableEditFBO.Visible = true;
            btnConfirmOperator.Visible = true;
            lblOperatorToEdit.Text = ddlOperatorToEdit.SelectedValue.ToString();
            //btnEditOperator.Visible = false;
            btnEditDefaultMO.Visible = false;
            btnTestDefaultMO.Visible = false;

            btnConfirmPlane.Visible = false;
            ddlPlaneOperator.Visible = false;

            //Second FBO id
            //56f219601f0066000f402c1e
            //causes error
            //WinnerCosts newDefaultMO = (WinnerCosts)Session["OperatorsMOs"];

            //lblOpPax1.Text = currentDefaultMO.pax1.ToString("P");
            //lblOpPax2.Text = currentDefaultMO.pax2.ToString("P");
            //lblOpPax3.Text = currentDefaultMO.pax2.ToString("P");
            //lblOpPax4.Text = currentDefaultMO.pax2.ToString("P");
            //lblOpPax5.Text = currentDefaultMO.pax2.ToString("P");
            //lblOpPax6.Text = currentDefaultMO.pax2.ToString("P");
            //lblOpPax7.Text = currentDefaultMO.pax2.ToString("P");
            //lblOpPax8.Text = currentDefaultMO.pax2.ToString("P");
            //lblOpPax9.Text = currentDefaultMO.pax2.ToString("P");
            //lblOpPax10.Text = currentDefaultMO.pax2.ToString("P");
            //txtOpPax1.Text = currentDefaultMO.pax1.ToString();
            //txtOpPax2.Text = currentDefaultMO.pax2.ToString();
            //txtOpPax3.Text = currentDefaultMO.pax2.ToString();
            //txtOpPax4.Text = currentDefaultMO.pax2.ToString();
            //txtOpPax5.Text = currentDefaultMO.pax2.ToString();
            //txtOpPax6.Text = currentDefaultMO.pax2.ToString();
            //txtOpPax7.Text = currentDefaultMO.pax2.ToString();
            //txtOpPax8.Text = currentDefaultMO.pax2.ToString();
            //txtOpPax9.Text = currentDefaultMO.pax2.ToString();
            //txtOpPax10.Text = currentDefaultMO.pax2.ToString();


            WinnerCosts newWinnerMO = (WinnerCosts)Session["DefaultMO"];
            //lblOpPax1.Text = newWinnerMO.fees.factors[0].ToString("P");
            lblOpPax2.Text = newWinnerMO.fees.factors[1].ToString("P");
            lblOpPax3.Text = newWinnerMO.fees.factors[2].ToString("P");
            lblOpPax4.Text = newWinnerMO.fees.factors[3].ToString("P");
            lblOpPax5.Text = newWinnerMO.fees.factors[4].ToString("P");
            lblOpPax6.Text = newWinnerMO.fees.factors[5].ToString("P");
            lblOpPax7.Text = newWinnerMO.fees.factors[6].ToString("P");
            lblOpPax8.Text = newWinnerMO.fees.factors[7].ToString("P");
            lblOpPax9.Text = newWinnerMO.fees.factors[8].ToString("P");
            lblOpPax10.Text = newWinnerMO.fees.factors[9].ToString("P");
            //txtOpPax1.Text = newWinnerMO.fees.factors[0].ToString();
            txtOpPax2.Text = newWinnerMO.fees.factors[1].ToString();
            txtOpPax3.Text = newWinnerMO.fees.factors[2].ToString();
            txtOpPax4.Text = newWinnerMO.fees.factors[3].ToString();
            txtOpPax5.Text = newWinnerMO.fees.factors[4].ToString();
            txtOpPax6.Text = newWinnerMO.fees.factors[5].ToString();
            txtOpPax7.Text = newWinnerMO.fees.factors[6].ToString();
            txtOpPax8.Text = newWinnerMO.fees.factors[7].ToString();
            txtOpPax9.Text = newWinnerMO.fees.factors[8].ToString();
            txtOpPax10.Text = newWinnerMO.fees.factors[9].ToString();


            //txtOpPax1.Visible = true;
            txtOpPax2.Visible = true;
            txtOpPax3.Visible = true;
            txtOpPax4.Visible = true;
            txtOpPax5.Visible = true;
            txtOpPax6.Visible = true;
            txtOpPax7.Visible = true;
            txtOpPax8.Visible = true;
            txtOpPax9.Visible = true;
            txtOpPax10.Visible = true;
            //lblOpPax1.Visible = false;
            lblOpPax2.Visible = false;
            lblOpPax3.Visible = false;
            lblOpPax4.Visible = false;
            lblOpPax5.Visible = false;
            lblOpPax6.Visible = false;
            lblOpPax7.Visible = false;
            lblOpPax8.Visible = false;
            lblOpPax9.Visible = false;
            lblOpPax10.Visible = false;

        }

        protected void btnConfirmOps_Click(object sender, EventArgs e)
        {
            //editMO newOperatorMO = (editMO)Session["OperatorMO"];
            WinnerCosts newOperatorMO = (WinnerCosts)Session["DefaultMO"];

            //if (!String.IsNullOrEmpty(txtOpPax1.Text))
            //{
            //    newOperatorMO.fees.factors[0] = Convert.ToDouble(txtAllPax1.Text);
            //    lblOpPax1.Text = newOperatorMO.fees.factors[0].ToString("P");
            //}
            if (!String.IsNullOrEmpty(txtOpPax2.Text))
            {
                newOperatorMO.fees.factors[1] = Convert.ToDouble(txtOpPax2.Text);
                lblOpPax2.Text = newOperatorMO.fees.factors[1].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtOpPax3.Text))
            {
                newOperatorMO.fees.factors[2] = Convert.ToDouble(txtOpPax3.Text);
                lblOpPax3.Text = newOperatorMO.fees.factors[2].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtOpPax4.Text))
            {
                newOperatorMO.fees.factors[3] = Convert.ToDouble(txtOpPax4.Text);
                lblOpPax4.Text = newOperatorMO.fees.factors[3].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtOpPax5.Text))
            {
                newOperatorMO.fees.factors[4] = Convert.ToDouble(txtOpPax5.Text);
                lblOpPax5.Text = newOperatorMO.fees.factors[4].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtOpPax6.Text))
            {
                newOperatorMO.fees.factors[5] = Convert.ToDouble(txtOpPax6.Text);
                lblOpPax6.Text = newOperatorMO.fees.factors[5].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtOpPax7.Text))
            {
                newOperatorMO.fees.factors[6] = Convert.ToDouble(txtOpPax7.Text);
                lblOpPax7.Text = newOperatorMO.fees.factors[6].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtOpPax8.Text))
            {
                newOperatorMO.fees.factors[7] = Convert.ToDouble(txtOpPax8.Text);
                lblOpPax8.Text = newOperatorMO.fees.factors[7].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtOpPax9.Text))
            {
                newOperatorMO.fees.factors[8] = Convert.ToDouble(txtOpPax9.Text);
                lblOpPax9.Text = newOperatorMO.fees.factors[8].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtOpPax10.Text))
            {
                newOperatorMO.fees.factors[9] = Convert.ToDouble(txtOpPax10.Text);
                lblOpPax10.Text = newOperatorMO.fees.factors[9].ToString("P");
            }

            sendUpdate fboUpdateToSend = new sendUpdate();
            fboUpdateToSend.id = "55a74297e4b07e03cc6ac65e";
            fboUpdateToSend.updates = newOperatorMO;

            var fboUpdateResponse = RestClientHelper.Post<editMO>("updateFbo", fboUpdateToSend, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
          //  var isSucess = fboUpdateResponse.Success.ToString();
            var success = JsonConvert.DeserializeObject<WinnerCosts>(fboUpdateResponse.FunctionResponse.ToString());


            //newOperatorMO.username = "operatorMO@MO.com";
            //var success = JsonConvert.DeserializeObject<editMO>(moOpResponse.FunctionResponse.ToString());

            //lblOpPax1.Visible = true;
            lblOpPax2.Visible = true;
            lblOpPax3.Visible = true;
            lblOpPax4.Visible = true;
            lblOpPax5.Visible = true;
            lblOpPax6.Visible = true;
            lblOpPax7.Visible = true;
            lblOpPax8.Visible = true;
            lblOpPax9.Visible = true;
            lblOpPax10.Visible = true;
            //txtOpPax1.Visible = false;
            txtOpPax2.Visible = false;
            txtOpPax3.Visible = false;
            txtOpPax4.Visible = false;
            txtOpPax5.Visible = false;
            txtOpPax6.Visible = false;
            txtOpPax7.Visible = false;
            txtOpPax8.Visible = false;
            txtOpPax9.Visible = false;
            txtOpPax10.Visible = false;

            btnEditOperator.Visible = false;
            btnConfirmOperator.Visible = false;
            ddlOperatorToEdit.SelectedIndex = -1;

            btnEditDefaultMO.Visible = true;
            btnTestDefaultMO.Visible = false;

            btnEditPlane.Visible = false;
            btnConfirmPlane.Visible = false;
            ddlPlaneOperator.Visible = true;
            TableEditFBO.Visible = false;
        }
        protected void btnEditOps_Click(object sender, EventArgs e)
        {

            btnConfirmOperator.Visible = true;
            btnEditOperator.Visible = false;
            btnEditDefaultMO.Visible = false;
            ddlPlaneOperator.Visible = false;
            ddlSelectPlane.Visible = false;
            TableEditSpecificPlane.Visible = false;

            //lblOpPax1.Visible = false;
            lblOpPax2.Visible = false;
            lblOpPax3.Visible = false;
            lblOpPax4.Visible = false;
            lblOpPax5.Visible = false;
            lblOpPax6.Visible = false;
            lblOpPax7.Visible = false;
            lblOpPax8.Visible = false;
            lblOpPax9.Visible = false;
            lblOpPax10.Visible = false;
            //txtOpPax1.Visible = true;
            txtOpPax2.Visible = true;
            txtOpPax3.Visible = true;
            txtOpPax4.Visible = true;
            txtOpPax5.Visible = true;
            txtOpPax6.Visible = true;
            txtOpPax7.Visible = true;
            txtOpPax8.Visible = true;
            txtOpPax9.Visible = true;
            txtOpPax10.Visible = true;
        }

        #endregion

        #region "Specific Plane Area"
        protected void ddlPlaneOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSelectPlane.Visible = true;
            lblOperatorPlane.Text = ddlPlaneOperator.SelectedValue.ToString();

            btnEditOperator.Visible = false;
            btnConfirmOperator.Visible = false;
            ddlOperatorToEdit.SelectedIndex = -1;
            ddlOperatorToEdit.Visible = false;

            btnEditDefaultMO.Visible = false;
            btnTestDefaultMO.Visible = false;

            btnEditPlane.Visible = false;
            btnConfirmPlane.Visible = false;
            ddlPlaneOperator.Visible = true;
        }
        protected void ddlSelectPlane_SelectedIndexChanged(object sender, EventArgs e)
        {
            TableEditSpecificPlane.Visible = true;
            lblPlaneToEdit.Text = ddlSelectPlane.SelectedValue.ToString();

            btnConfirmOperator.Visible = false;            
            lblPlaneToEdit.Text = ddlSelectPlane.SelectedValue.ToString();
            btnEditOperator.Visible = false;
            btnEditDefaultMO.Visible = false;
            btnConfirmPlane.Visible = true;

            //txtPlanePax1.Visible = true;
            txtPlanePax2.Visible = true;
            txtPlanePax3.Visible = true;
            txtPlanePax4.Visible = true;
            txtPlanePax5.Visible = true;
            txtPlanePax6.Visible = true;
            txtPlanePax7.Visible = true;
            txtPlanePax8.Visible = true;
            txtPlanePax9.Visible = true;
            txtPlanePax10.Visible = true;
            //lblPlanePax1.Visible = false;
            lblPlanePax2.Visible = false;
            lblPlanePax3.Visible = false;
            lblPlanePax4.Visible = false;
            lblPlanePax5.Visible = false;
            lblPlanePax6.Visible = false;
            lblPlanePax7.Visible = false;
            lblPlanePax8.Visible = false;
            lblPlanePax9.Visible = false;
            lblPlanePax10.Visible = false;

            if (ddlPlaneOperator.SelectedIndex > -1)
            {
                if (ddlSelectPlane.SelectedIndex > -1)
                {
                    TableEditSpecificPlane.Visible = true;
                }
            }
        }             

        protected void btnConfirmPlane_Click(object sender, EventArgs e)
        {
            //doesnt work
            Aircraft newPlaneMO = (Aircraft)Session["CurrentPlaneMO"];
            //var newPlaneMO = Session["CurrentPlaneMO"];


            //if (!String.IsNullOrEmpty(txtPlanePax1.Text))
            //{
            //    newPlaneMO.fees.factors[0] = Convert.ToDouble(txtPlanePax1.Text);
            //    lblPlanePax1.Text = newPlaneMO.fees.factors[0].ToString("P");
            //}
            if (!String.IsNullOrEmpty(txtPlanePax2.Text))
            {
                newPlaneMO.fees.factors[1] = Convert.ToDouble(txtPlanePax2.Text);
                lblPlanePax2.Text = newPlaneMO.fees.factors[1].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtPlanePax3.Text))
            {
                newPlaneMO.fees.factors[2] = Convert.ToDouble(txtPlanePax3.Text);
                lblPlanePax3.Text = newPlaneMO.fees.factors[2].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtPlanePax4.Text))
            {
                newPlaneMO.fees.factors[3] = Convert.ToDouble(txtPlanePax4.Text);
                lblPlanePax4.Text = newPlaneMO.fees.factors[3].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtPlanePax5.Text))
            {
                newPlaneMO.fees.factors[4] = Convert.ToDouble(txtPlanePax5.Text);
                lblPlanePax5.Text = newPlaneMO.fees.factors[4].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtPlanePax6.Text))
            {
                newPlaneMO.fees.factors[5] = Convert.ToDouble(txtPlanePax6.Text);
                lblPlanePax6.Text = newPlaneMO.fees.factors[5].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtPlanePax7.Text))
            {
                newPlaneMO.fees.factors[6] = Convert.ToDouble(txtPlanePax7.Text);
                lblPlanePax7.Text = newPlaneMO.fees.factors[6].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtPlanePax8.Text))
            {
                newPlaneMO.fees.factors[7] = Convert.ToDouble(txtPlanePax8.Text);
                lblPlanePax8.Text = newPlaneMO.fees.factors[7].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtPlanePax9.Text))
            {
                newPlaneMO.fees.factors[8] = Convert.ToDouble(txtPlanePax9.Text);
                lblPlanePax9.Text = newPlaneMO.fees.factors[8].ToString("P");
            }
            if (!String.IsNullOrEmpty(txtPlanePax10.Text))
            {
                newPlaneMO.fees.factors[9] = Convert.ToDouble(txtPlanePax10.Text);
                lblPlanePax10.Text = newPlaneMO.fees.factors[9].ToString("P");
            }

            sendUpdate sendAircraftUpdate = new sendUpdate();
            sendAircraftUpdate.id = "55a74198e4b07e03cc6ac65a";
            sendAircraftUpdate.updates = newPlaneMO;
            //var factorsToSend = new AircraftFees();
            //factorsToSend.factors = newPlaneMO.fees.factors;
            //factorsToSend.hourlyRate = 1450;
            //sendAircraftUpdate.updates = newPlaneMO.fees.factors;


            //sendAircraftUpdate.updates = factorsToSend;
            

           var moPlaneResponse = RestClientHelper.Post<editMO>("updatePlane", sendAircraftUpdate, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
            var success = JsonConvert.DeserializeObject<Aircraft>(moPlaneResponse.FunctionResponse.ToString());

            //lblPlanePax1.Visible = true;
            lblPlanePax2.Visible = true;
            lblPlanePax3.Visible = true;
            lblPlanePax4.Visible = true;
            lblPlanePax5.Visible = true;
            lblPlanePax6.Visible = true;
            lblPlanePax7.Visible = true;
            lblPlanePax8.Visible = true;
            lblPlanePax9.Visible = true;
            lblPlanePax10.Visible = true;
            //txtPlanePax1.Visible = false;
            txtPlanePax2.Visible = false;
            txtPlanePax3.Visible = false;
            txtPlanePax4.Visible = false;
            txtPlanePax5.Visible = false;
            txtPlanePax6.Visible = false;
            txtPlanePax7.Visible = false;
            txtPlanePax8.Visible = false;
            txtPlanePax9.Visible = false;
            txtPlanePax10.Visible = false;

            btnConfirmPlane.Visible = false;
            btnEditPlane.Visible = false;
            TableEditSpecificPlane.Visible = false;
            ddlSelectPlane.Visible = false;
            ddlSelectPlane.SelectedIndex = -1;
            ddlPlaneOperator.SelectedIndex = -1;

            btnConfirmOperator.Visible = false;           
            ddlOperatorToEdit.Visible = true;

            btnEditDefaultMO.Visible = true;
            btnTestDefaultMO.Visible = false;

            btnConfirmPlane.Visible = false;
            ddlPlaneOperator.Visible = true;
        }
        protected void btnEditPlane_Click(object sender, EventArgs e)
        {
            //lblPlanePax1.Visible = false;
            lblPlanePax2.Visible = false;
            lblPlanePax3.Visible = false;
            lblPlanePax4.Visible = false;
            lblPlanePax5.Visible = false;
            lblPlanePax6.Visible = false;
            lblPlanePax7.Visible = false;
            lblPlanePax8.Visible = false;
            lblPlanePax9.Visible = false;
            lblPlanePax10.Visible = false;
            //txtPlanePax1.Visible = true;
            txtPlanePax2.Visible = true;
            txtPlanePax3.Visible = true;
            txtPlanePax4.Visible = true;
            txtPlanePax5.Visible = true;
            txtPlanePax6.Visible = true;
            txtPlanePax7.Visible = true;
            txtPlanePax8.Visible = true;
            txtPlanePax9.Visible = true;
            txtPlanePax10.Visible = true;

            btnEditPlane.Visible = false;
            btnConfirmPlane.Visible = true;

            btnEditOperator.Visible = false;
            btnConfirmOperator.Visible = false;
            ddlOperatorToEdit.SelectedIndex = -1;
            ddlOperatorToEdit.Visible = false;

            btnEditDefaultMO.Visible = false;
            btnTestDefaultMO.Visible = false;

            btnEditPlane.Visible = false;
            btnConfirmPlane.Visible = false;
            ddlPlaneOperator.Visible = true;
        }
        #endregion

        #region "Test Area"
        protected void btnConfirmDefaultMO_Click(object sender, EventArgs e)
        {

        }

        
        
        
        #endregion
        }
}