﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Helpers;
using SimplePortal.Objects;
using Newtonsoft.Json;

namespace SimplePortal.AdminArea
{
    public partial class UserAdministration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ////Hide the screen if Access Group doesn't match
            ////stops people from changing url and accessing directly 
            //var userAccessGroup = Session["LoggedInUserAccessGroup"];
            ////if (Session["LoggedInUserAccessGroup"] == null)

            //Page.Master.FindControl("MainContent").Visible = false;
            //if (userAccessGroup == null)
            //{
            //    Page.Master.FindControl("MainContent").Visible = false;
            //}
            //else if (userAccessGroup.ToString() == "winner")
            //{
            //    Page.Master.FindControl("MainContent").Visible = true;
            //}
            //else if (userAccessGroup.ToString() == "flytzeAdmin")
            //{
            //    Page.Master.FindControl("MainContent").Visible = true;
            //}

            //function to get list of users and their current accessGroup

            //Object getNewUsers = new Object();

            //var getNewUsersResponse = RestClientHelper.Post<WinnerCosts>("findAccounts", getNewUsers, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
            ////var getNewUsersResponse = RestClientHelper.Post<AccountCollection>("readNewUsers", getNewUsers, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
            //var newUsers = JsonConvert.DeserializeObject<WinnerCosts>(getNewUsersResponse.FunctionResponse.ToString());

            //ddlSelectUser.DataSource = newUsers;
            ////ddlSelectUser.DataTextField = "Name";
            ////ddlSelectUser.DataValueField = "ID";
            //ddlSelectUser.DataBind();
        }


        protected void btnEditUserGroup_Click(object sender, EventArgs e)
        {

            //var updateUser = new AccountCollection();
            var updateUser = new UpdateAccessGroup();
            //updateUser.username = txtUsername.Text.ToString();
            //updateUser.accessGroup = txtAccessGroup.Text.ToString();

            //updateUser.username = ddlSelectUser.SelectedValue.ToString();
            //updateUser.accessGroup = ddlSelectAccessGroup.SelectedValue.ToString();

            var updateUserResponse = RestClientHelper.Post<AccountCollection>("adminEditUser", updateUser, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
            // = JsonConvert.DeserializeObject<WinnerCosts>(updateUserResponse.FunctionResponse.ToString());

            lblUpdateResponse.Text = updateUserResponse.FunctionResponse.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //1. User performs action (ex. input user info, click button to submit)

            //2. Get saltSecure for every action
            //saltUser -> encode-> createHash = saltSecure
            var salt = Security.Salt(20);
            var newSaltUser = Security.saltUser(salt);
            var saltUserEncoded = Security.encodeUTF8(newSaltUser);
            Security.createHash(saltUserEncoded);

            //3. Build Json request
            //**assumptions: funciton = registerUser
            WallRequest registerUser = new WallRequest();



            //4. Encode Json request

            //5. hash Json request

            //6. Build Json requestSecure

            //7. POST Json requestSecure to Wall


        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var newRegisterUser = new registerNewUser();


            newRegisterUser.username = TextBox1.Text;
            newRegisterUser.password= TextBox2.Text;
            newRegisterUser.firstName = TextBox3.Text;
            newRegisterUser.lastName = TextBox4.Text;
            newRegisterUser.phoneNumber = Convert.ToInt64(TextBox5.Text);
            //newRegisterUser.dateOfBirth.ToString("yyyy-MM-ddThh:mm:ssZ") = txtDoB.Text;
            //newRegisterUser.dateOfBirth = txtDoB.Text.ToString("%Y-%m-%dT%H:%M:%SZ");
            //newRegisterUser.dateOfBirth = Convert.ToDateTime(txtDoB.Text).ToString();
            newRegisterUser.dateOfBirth = "1989-08-30T00:00:00Z";
          //  newRegisterUser.phoneNumber = Convert.ToInt64(txtPhoneNumber.Text);
            newRegisterUser.middleName = "";
            newRegisterUser.iosToken = "";
            newRegisterUser.type = "unassigned";
            newRegisterUser.gender = "unassigned";
            newRegisterUser.weight = 0;

            var newUser = RestClientHelper.Post<registerNewUser[]>("registerUser", newRegisterUser, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());

        }

        //protected void ddlSelectUser_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    lblSelectUser.Text= ddlSelectUser.SelectedValue.ToString();
        //}

        //protected void ddlSelectAccessGroup_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    lblAccessGroup.Text = ddlSelectAccessGroup.SelectedValue.ToString();
        //}



    }
}