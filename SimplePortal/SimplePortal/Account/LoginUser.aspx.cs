﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Objects;
using SimplePortal.Helpers;
using Newtonsoft.Json;
using System.Configuration;
using SimplePortal.Common;

namespace SimplePortal.Account
{
    public partial class LoginUser : System.Web.UI.Page
    {
        private static string ServiceUserToken = ConfigurationManager.AppSettings[Constants.FlytzeSystemTokenKey];

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                lblResponseToUser.Text = "Password field is required.";
            }
            else {
                //TODO encrypt password in transit, it is sent in plaintext in requestSecureToSend
                var UserToLogin = new LoginUserParameters();
                UserToLogin.username = txtEmail.Text.ToString();
                UserToLogin.password = txtPassword.Text.ToString();
                var validateUser = RestClientHelper.Post<LoginUserParameters>("login", UserToLogin, ServiceUserToken, "system@flytze.com");                

                if (validateUser.FunctionResponse.ToString() != null)
                {
                    LoginUserParameters loggedIn = JsonConvert.DeserializeObject<LoginUserParameters>(validateUser.FunctionResponse.ToString());
                    if (validateUser.Success == true)
                    {

                        Session["LoggedInUserToken"] = loggedIn.token.ToString();
                        Session["LoggedInUser"] = txtEmail.Text;
                        if (!String.IsNullOrEmpty(loggedIn.accessGroup)) {  
                         Session["LoggedInUserAccessGroup"] = loggedIn.accessGroup.ToString();
                        }

                        this.Page.Master.FindControl("HelloUser").Visible = true;
                        this.Page.Master.FindControl("Anonymous").Visible = false;

                        //Label lblHelloUser = Page.Master.FindControl("HelloUser").FindControl("lblHelloUser") as Label;
                        //lblHelloUser.Text = txtEmail.Text.ToString();

                        LinkButton lblHelloUser = Page.Master.FindControl("HelloUser").FindControl("lblHelloUser") as LinkButton;
                        lblHelloUser.Text = txtEmail.Text.ToString();

                        Response.Redirect("~/Default.aspx");
                    }
                    else
                    {
                        lblResponseToUser.Text = "Invalid credentials entered.";
                    }
                }
                else { lblResponseToUser.Text = "Login failed: " + validateUser.Error.ToString(); }

                //"Login failed: Incorrect email/password combination."; }
            }
        }

        protected void ResetPw(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtEmail.Text))
            {
                lblResponseToUser.Text = "Please enter email in Email/Username field to receive new password in your inbox.";
            }


            txtPassword.Text = "none";

            var email = txtEmail.Text.ToString();
            var emailToReset = new RequestByUsername();
            emailToReset.username = email;

            var viewThisUser = new RequestByUsername();
            viewThisUser.username = email;

            //var viewUserResponse = RestClientHelper.Post<AccountCollection>("adminViewUser", viewThisUser, "92sWJIOMyHgLsesNJtHE0Cdut", "general");   
            //var viewUserResponse = RestClientHelper.Post<AccountCollection>("viewUser", viewThisUser, "E6b2UP2CDn7n0xo79FC1TDOm4", "david@intraspire.com");
            //var viewUser = JsonConvert.DeserializeObject<AccountCollection>(viewUserResponse.FunctionResponse.ToString());


            var resetPwCall = RestClientHelper.Post<AccountCollection>("requestPasswordReset", emailToReset, ServiceUserToken, "system@flytze.com");
            //var resetPwCall = RestClientHelper.Post<AccountCollection>("resetPassword", emailToReset, viewUser.wallToken.ToString(), email);
            var resetPwResponse = JsonConvert.DeserializeObject<AccountCollection>(resetPwCall.FunctionResponse.ToString());
            //lblResponseToUser.Text = resetPwResponse.FunctionResponse.ToString();
            lblResponseToUser.Text = resetPwCall.FunctionResponse.ToString();
        }
    }
}