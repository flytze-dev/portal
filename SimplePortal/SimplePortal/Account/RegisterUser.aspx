﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Portal.Master" AutoEventWireup="true" CodeBehind="RegisterUser.aspx.cs" Inherits="SimplePortal.Account.RegisterUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle" runat="server">
    Register New User
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-horizontal">
        <h4>Create a new account</h4>
        
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtEmail" CssClass="col-md-2 control-label" style="left: 0px; top: 0px">Username (email)</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail"
                    CssClass="text-danger" ErrorMessage="The email field is required." />
            </div>
        </div>

        <%--<asp:ValidationSummary runat="server" CssClass="text-danger" />--%>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtFName" CssClass="col-md-2 control-label">First Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtFName" CssClass="form-control"  />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtFName"
                    CssClass="text-danger" ErrorMessage="First Name is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtLName" CssClass="col-md-2 control-label">Last Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtLName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtLName"
                    CssClass="text-danger" ErrorMessage="Last Name is required." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtPassword" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPassword"
                    CssClass="text-danger" ErrorMessage="The password field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
            </div>
        </div>

<%--                <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtDoB" CssClass="col-md-2 control-label">Date of Birth</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtDoB" CssClass="form-control" TextMode="Date" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDoB"
                    CssClass="text-danger" ErrorMessage="Please enter Date of Birth." />
            </div>
        </div>--%>
                <div class="form-group">
                    <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Label runat="server" AssociatedControlID="txtPhoneNumber" CssClass="col-md-2 control-label">Phone Number</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtPhoneNumber" CssClass="form-control" TextMode="Number"/>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPhoneNumber"
                    CssClass="text-danger" ErrorMessage="Phone Number is required." />
            </div>
        </div>
<%--                <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtType" CssClass="col-md-2 control-label">Type</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtType" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtType"
                    CssClass="text-danger" ErrorMessage="Type field is required." />
            </div>
        </div>  --%>




        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="button" />
                
            </div>
        </div>
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator"></asp:CustomValidator>
    </div>

<%--    <asp:Label ID="lblError" runat="server" Text="Label"></asp:Label> <br/>
    <asp:Label ID="lblEventId" runat="server" Text="Label"></asp:Label> <br/>
    <asp:Label ID="lblSuccess" runat="server" Text="Label"></asp:Label> --%>
    <asp:Label ID="lblFunctionResponse" runat="server" Text="Label"></asp:Label> <br/>


</asp:Content>
