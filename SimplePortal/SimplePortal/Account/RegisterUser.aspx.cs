﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Helpers;
using SimplePortal.Objects;
using Newtonsoft.Json;
using SimplePortal.Common;
using System.Configuration;

namespace SimplePortal.Account
{
    public partial class RegisterUser : System.Web.UI.Page
    {
        private static string FlytzeSystemTokenKey = ConfigurationManager.AppSettings[Constants.FlytzeServiceUrlKey];

        protected void Page_Load(object sender, EventArgs e)
        {
            lblFunctionResponse.Visible = false;
        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {

            var newRegisterUser = new registerNewUser();

             newRegisterUser.firstName = txtFName.Text;
             newRegisterUser.lastName = txtLName.Text;
             newRegisterUser.username = txtEmail.Text;
             newRegisterUser.password = txtPassword.Text;
            //newRegisterUser.dateOfBirth.ToString("yyyy-MM-ddThh:mm:ssZ") = txtDoB.Text;
            //newRegisterUser.dateOfBirth = txtDoB.Text.ToString("%Y-%m-%dT%H:%M:%SZ");
            //newRegisterUser.dateOfBirth = Convert.ToDateTime(txtDoB.Text).ToString();
            newRegisterUser.dateOfBirth = "1989-08-30T00:00:00Z";
             newRegisterUser.phoneNumber = Convert.ToInt64(txtPhoneNumber.Text);
             newRegisterUser.middleName = "";
             newRegisterUser.iosToken = "";
             newRegisterUser.type = "unassigned";
            newRegisterUser.gender = "unassigned";
            newRegisterUser.weight = 0;
            newRegisterUser.accessGroup = "unassigned";

            //User username and not email

           var newUser = RestClientHelper.Post<registerNewUser[]>("registerUser", newRegisterUser, FlytzeSystemTokenKey, "system@flytze.com");
            LoginUserParameters loggedIn = JsonConvert.DeserializeObject<LoginUserParameters>(newUser.FunctionResponse.ToString());


            //Hash test, dont need
            //lblFunctionResponse.Text = Security.encodeAndHash(newRegisterUser);
            //var requestSecureToSend = JsonConvert.SerializeObject(newRegisterUser);
            //var hashedJSONrequest = Security.encodeAndHash(requestSecureToSend);

            //lblSuccess.Text = requestSecureToSend.ToString();
            //lblFunctionResponse.Text = hashedJSONrequest;

            lblFunctionResponse.Visible = true;

            if (newUser.Success == true)
            {
                
                //lblEventId.Text = newUser.EventId.ToString();
                lblFunctionResponse.Text = newUser.FunctionResponse.ToString();
                //lblSuccess.Text = newUser.Success.ToString();

                Session["LoggedInUser"] = txtEmail.Text;
                Session["LoggedInUserToken"] = loggedIn.token.ToString();
                //Session["LoggedInUserAccessGroup"] = loggedIn.accessGroup.ToString();
                Session["LoggedInUserAccessGroup"] = "unassigned";

                Response.Redirect("~/Default.aspx");
                //lblFunctionResponse.Text = "SUCCESS";
            }
            else
            {
                lblFunctionResponse.Text = newUser.Error.ToString();
            }

            //Response.Redirect("~/Account/LoginUser.aspx");
        }
    }
}