﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using System.Resources;
using RestSharp;
using SimplePortal.Objects;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using SimplePortal.Common;


namespace SimplePortal.Helpers
{
    public class RestClientHelper
    {
        /// <summary>
        /// rest client helper
        /// </summary>
        /// 
        #region  " Private Methods "
        private static string FlytzeServiceUrl = ConfigurationManager.AppSettings[Constants.FlytzeServiceUrlKey];


        //Production url:
        //private static string FlytzeServiceUrl = "https://service.flytze.com/thewall";
        //doesn't work
        //        private static string FlytzeServiceUrl = "https://flytzecorp.com/thewall";
        //private static string FlytzeServiceUrl = "http://13.92.245.71/thewall";
        //13.92.245.71/flytze.eastus.cloudapp.azure.com
        //private static string FlytzeServiceUrl = "https://flytzecorp.com/thewall";

        //Dev url:
        //doesn't work
        //private static string FlytzeServiceUrl = "http://flytzedev.azurewebsites.net/thewall";
        //works not working
        //private static string FlytzeServiceUrl = "https://65.52.245.95:5050/thewall";
        //        private static string FlytzeServiceUrl = "http://65.52.245.95:5050/thewall";
        //      private static string FlytzeServiceUrl = "https://service.flytze.com/thewall";
        //private static string FlytzeServiceUrl = "http://flytze.azurewebsites.net";

        //Local Test url--this works!
        //must -360
        //        private static string FlytzeServiceUrl = "http://localhost:55555/thewall";


        //private static string FlytzeServiceUrl2 = "http://flytze.azurewebsites.net/wallpass";
        //ConfigurationManager.AppSettings["Flytze.Service.Url"];

        #endregion

        #region " Public Methods "          
        ///        /// <summary>
        /// post to a resource
        /// </summary>
        //public static WallResponse<T1> Post<T1>( string function, object request)



        //        public static WallResponse<T1> Post<T1>(string function, object request)
        public static WallResponse<T1> Post<T1>(string function, object request,string LoggedInUserToken, string usernameSendingRequest)
            //!!!!would it be better to have LoggedInUserToken and usernameSendingRequest as part of the request instead of broken out?
        {
            //var LoggedInUserToken = Session["LoggedInUserToken"];

            var client = new RestClient(FlytzeServiceUrl);
            var sameSalt = Security.Salt(15);
            //var sameSalt = "bM3AGCTyWCipamp";
            //var usernameSendingRequest = "system@simpleportal.com";            

            // DO: build the wall request
            var wallRequest = new WallRequest();
            wallRequest.function = function;
            wallRequest.functionParams = request;
            wallRequest.pepper = "sdeODQ0T25oEBOvZW72xOXrOVXZOkz1HWNt9Now4DJ0EEdWXeA0ctGXbqqlxKuJp3H7MSyBK6BAt1DKipXNVMmSx0H";
            wallRequest.saltSecure = Security.saltSecure(sameSalt, usernameSendingRequest);
            //wallRequest.saltSecure = "f548e9f385edca4578ee8a0c3ba1b93bd2e1e868c74b8c0070c30a5182cede06";
//            wallRequest.sentTime = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
            wallRequest.sentTime = DateTime.UtcNow.AddMinutes(-300).ToString("yyyy-MM-ddTHH:mm:ssZ");
            //wallRequest.sentTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
            wallRequest.token = LoggedInUserToken;
            wallRequest.username = usernameSendingRequest;

           // var toEncode = JsonConvert.SerializeObject(wallRequest);

            var toEncode = JsonConvert.SerializeObject(wallRequest,
                            Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
            var hashedRequest = Security.encodeAndHash(toEncode);
            //var hashedRequest = "3145d510bd95b118c8f2c91611deb0757e8cc71a67f1a3effa9c4273d5b7ee8f";

            //DO: build the secure wall request
            var wallSecure = new WallRequestSecure();
            wallSecure.username = wallRequest.username;
            wallSecure.sentTime = wallRequest.sentTime;
            wallSecure.function = wallRequest.function;
            wallSecure.functionParams = wallRequest.functionParams;
            wallSecure.hash = hashedRequest;            
            wallSecure.salt = sameSalt;
            
            var requestSecureToSend = JsonConvert.SerializeObject(wallSecure);

            var wallResponse = new WallResponse<T1>();
            var req = new RestRequest(Method.POST);
            req.RequestFormat = DataFormat.Json;
            req.AddHeader("Content-Type", "application/json");            
            var serializedBody = JsonConvert.SerializeObject(wallSecure,
                            Formatting.None,
                            new JsonSerializerSettings
                            {   
                                NullValueHandling = NullValueHandling.Ignore
                            });
            
            var deserializedObject = new JavaScriptSerializer().Deserialize<dynamic>(serializedBody.ToString());
            req.AddBody(deserializedObject);            
            var response = client.Execute(req);
           return JsonConvert.DeserializeObject<WallResponse<T1>>(response.Content);
         
        }

        public static T1 Delete<T1>(string resource)
        {
            var client = new RestClient(FlytzeServiceUrl);
            var req = new RestRequest(resource, Method.DELETE);
            var response = client.Execute(req);
            return JsonConvert.DeserializeObject<T1>(response.Content);
        }
        /// <summary>
        /// read to a resource
        /// </summary>
        public static T1 Get<T1>(string resource)
        {
            var client = new RestClient(FlytzeServiceUrl);
            var req = new RestRequest(resource, Method.GET);
            var response = client.Execute(req);
            return JsonConvert.DeserializeObject<T1>(response.Content);
        }
        /// <summary>
        /// post to a resource
        /// </summary>
        public static T1 Put<T1>(string resource, object request)
        {
            var client = new RestClient(FlytzeServiceUrl);
            var req = new RestRequest(resource, Method.PUT);
            req.AddObject(request);
            var response = client.Execute(req);
            return JsonConvert.DeserializeObject<T1>(response.Content);
        }
        #endregion
    }
}