﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using System.Resources;
using RestSharp;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using SimplePortal.Objects;

namespace SimplePortal.Helpers
{
    public class Security
    {
        //1. generate random string for salt 
        public static string Salt(int length)
        {
            Random _random = new Random(Environment.TickCount);
            string chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder salt1 = new StringBuilder(length);

            for (int i = 0; i < length; ++i)
                salt1.Append(chars[_random.Next(chars.Length)]);
            return salt1.ToString();
        }

        //2. combine with username for saltSecure
        public static string saltSecure(string salt, string username)
        {
            var saltUserValue = salt + username;
            var saltSecure = encodeAndHash(saltUserValue);
            return saltSecure;
        }

        //3. encode and hash for saltSecure and hash parameter
        public static string encodeAndHash(string toHash)
        {
            SHA256Managed crypt = new SHA256Managed();
            StringBuilder hash = new StringBuilder();
            //byte[] crypto = crypt.ComputeHash(toHash);
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(toHash));
            //byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
















        public static string hashObj (string toHash)
        {
            SHA256Managed crypt = new SHA256Managed();
            StringBuilder hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(toHash));
            //byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }




        //2. Append username to end of Salt using saltUser(Salt(20))
        public static string saltUser(string salt)
        {
            //Get user logged into Portal
            var userName = "admin@flytze.com";
            //var userName = HttpContext.Current.User.Identity.Name.ToString();
            var saltUserValue = salt + userName;
            return saltUserValue;
            //var saltUserValue = "QXV8U86GQ64HVGUM4Qbrycesublette@gmail.com";
        }
        //salt+username encode sha256
        //2.1     //encode in UTF-8
        public static string encodeUTF8(string toEncode)
        {
            var bytes = Encoding.UTF8.GetBytes(toEncode);
            return Encoding.UTF8.GetString(bytes);
            //b'QXV8U86GQ64HVGUM4Qbrycesublette@gmail.com'
        }



        //3. Create SHA Hash from saltUser to get saltSecure using createHash(saltUser(Salt(20)))
        public static string createHash(string toHash)
        {
                byte[] bytes = Encoding.UTF8.GetBytes(toHash);
                SHA256Managed hashstring = new SHA256Managed();
                byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;

            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
    




}
}