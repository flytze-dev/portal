﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using SimplePortal.Results;

namespace SimplePortal.Objects
{
    //this class structure is based off of trips objects in MongoDB
    public class Trip
    {
        [JsonProperty("actualCost")]
        public ActualTotalCost actualCost { get; set; }
        [JsonProperty("agreement")]
        public string agreement { get; set; }
        [JsonProperty("aircraft")]
        public Aircraft aircraft { get; set; }
        [JsonProperty("arrival")]
        public Arrival Arrival { get; set; }
        [JsonProperty("createdDateTime")]
        public DateTime createdDateTime { get; set; }
        [JsonProperty("departure")]
        public Departure departure { get; set; }        
        [JsonProperty("fbo")]
        public FBO fbo { get; set; }
        [JsonProperty("flightDistance")]
        public FlightDistance flightDistance { get; set; }
        [JsonProperty("flightTime")]
        public FlightTime flightTime { get; set; }
        [JsonProperty("flytzeId")]
        public string flytzeId { get; set; }
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("isFlytze")]
        public bool isFlytze { get; set; }
        [JsonProperty("isVerified")]
        public bool isVerified { get; set; }
        [JsonProperty("mo")]
        public MO mo { get; set; }
        [JsonProperty("moBreakdown")]
        //Object Array
        public MOBreakdown moBreakdown { get; set; }
        [JsonProperty("notes")]
        public string notes { get; set; }
        [JsonProperty("openSeats")]
        public int openSeats { get; set; }
        [JsonProperty("passengers")]
        public Passengers[] passengers { get; set; }
        [JsonProperty("privacy")]
        public Privacy privacy { get; set; }
        [JsonProperty("quote")]
        public Quote quote { get; set; }
        [JsonProperty("status")]
        public long status { get; set; }
        [JsonProperty("totalCost")]
        public TotalCost totalCost { get; set; }
    }

    public class TotalCost
    {
        [JsonProperty("airportConcession")]
        public double airportConcession { get; set; }
        [JsonProperty("baseRate")]
        public double baseRate { get; set; }
        [JsonProperty("dailyCrewCost")]
        public double dailyCrewCost { get; set; }
        [JsonProperty("flightCharges")]
        public double flightCharges { get; set; }
        [JsonProperty("fuelSurcharge")]
        public double fuelSurcharge { get; set; }
        [JsonProperty("miscFee")]
        public double miscFee { get; set; }
        [JsonProperty("segmentFee")]
        public double segmentFee { get; set; }
        [JsonProperty("subTotal")]
        public double subTotal { get; set; }
        //[JsonProperty("taxes")]
        //public Taxes taxes { get; set; }
        //[JsonProperty("taxes")]
        //public double taxes { get; set; }
        [JsonProperty("taxes")]
        public object taxes { get; set; }

        [JsonProperty("total")]
        public double total { get; set; }
    }

    public class Passengers
    {
        [JsonProperty("accountId")]
        public string accountId { get; set; }
        [JsonProperty("bookedDateTime")]
        public object bookedDateTime { get; set; }
        [JsonProperty("dateOfBirth")]
        public object dateOfBirth { get; set; }
        [JsonProperty("gender")]
        public string gender { get; set; }
        [JsonProperty("isInitiator")]
        public bool isInitiator { get; set; }
        [JsonProperty("luggage")]
        public Luggage luggage { get; set; }    
        [JsonProperty("name")]
        public Name name { get; set; }
        [JsonProperty("payment")]
        public Payment payment { get; set; }
        [JsonProperty("phone")]
        public string phone { get; set; }
        [JsonProperty("seatPosition")]
        public int seatPosition { get; set; }
        [JsonProperty("weight")]
        public long weight { get; set; }
    }

    public class Name
    {
        [JsonProperty("firstName")]
        public string fName { get; set; }
        [JsonProperty("lastName")]
        public string lName { get; set; }
        [JsonProperty("fullName")]
        public string fullName { get; set; }
    }

    public class Luggage
    {
        [JsonProperty("weight")]
        public long weight { get; set; }
        [JsonProperty("count")]
        public long count { get; set; }
    }

    public class Payment
    {
        //[JsonProperty("amount")]
        //public Details amount { get; set; }
        [JsonProperty("amount")]
        public long amount { get; set; }
        [JsonProperty("hasPaid")]
        public bool hasPaid { get; set; }
        [JsonProperty("method")]
        public string method { get; set; }
        [JsonProperty("transactionId")]
        public object transactionId { get; set; }
    }

    public class Details
    {
        [JsonProperty("token")]
        public object token { get; set; }
        [JsonProperty("amount")]
        public object amount { get; set; }
        [JsonProperty("gateway")]
        public object gateway { get; set; }
        [JsonProperty("method")]
        public object method { get; set; }
    }

    public class FlightTime
    {
        [JsonProperty("estimated")]
        public object Estimated { get; set; }
        [JsonProperty("actual")]
        public object Actual { get; set; }
    }

    public class Quote
    {
        [JsonProperty("aircraft")]
        public Aircraft Aircraft { get; set; }
        [JsonProperty("arrival")]
        public Arrival Arrival { get; set; }
        [JsonProperty("departure")]
        public Departure Departure { get; set; }
        [JsonProperty("fbo")]
        public FBO FBO { get; set; }
        [JsonProperty("flightDistance")]
        public double flightDistance { get; set; }
        [JsonProperty("flightTime")]
        public object FlightTime { get; set; }
        [JsonProperty("fuelSurcharge")]
        public FuelSurcharge FuelSurcharge { get; set; }
        [JsonProperty("passengerCount")]
        public object PassengerCount { get; set; }
        [JsonProperty("position")]
        public object Position { get; set; }
        [JsonProperty("totalCost")]
        public object TotalCost { get; set; }
    }

    public class FuelSurcharge
    {
        [JsonProperty("flightTime")]
        public double flightTime { get; set; }
        [JsonProperty("fuelSurcharge")]
        public double fuelSurcharge { get; set; }
        [JsonProperty("taxiTime")]
        public double taxiTime { get; set; }
        [JsonProperty("total")]
        public double total { get; set; }
    }

    public class Departure
    {
        [JsonProperty("airportCode")]
        public object AirportCode { get; set; }
        [JsonProperty("city")]
        public object city { get; set; }
        [JsonProperty("estimated")]
        public object Estimated { get; set; }
        [JsonProperty("stateName")]
        public object stateName { get; set; }
    }

    public class Arrival
    {
        [JsonProperty("airportCode")]
        public object AirportCode { get; set; }
        [JsonProperty("city")]
        public object city { get; set; }
        [JsonProperty("estimated")]
        public object Estimated { get; set; }
        [JsonProperty("stateName")]
        public object stateName { get; set; }
    }

    public class Privacy
    {
        [JsonProperty("privacy")]
        public List<string> privacy { get; set; }
    }

    public class FlightDistance
    {
        [JsonProperty("estimated")]
        public double estimated { get; set; }
        [JsonProperty("actual")]
        public object Actual { get; set; }
    }

    public class TripSearch
    {
        public string flytzeId { get; set; }

    }

    public class AppSearchTrip
    {
        [JsonProperty("departure")]
        public Departure departure { get; set; }
        [JsonProperty("arrival")]
        public Arrival arrival { get; set; }
    }

    public class MOBreakdown
    {
        //an object of arrays, Passengers[] is an array of objects
        [JsonProperty("0")]
        public breakdownCostObject[] breakdownPaxPosition0 { get; set; }
        [JsonProperty("1")]
        public breakdownCostObject[] breakdownPaxPosition1 { get; set; }
        [JsonProperty("2")]
        public breakdownCostObject[] breakdownPaxPosition2 { get; set; }
        [JsonProperty("3")]
        public breakdownCostObject[] breakdownPaxPosition3 { get; set; }
        [JsonProperty("4")]
        public breakdownCostObject[] breakdownPaxPosition4 { get; set; }
        [JsonProperty("5")]
        public breakdownCostObject[] breakdownPaxPosition5 { get; set; }
        [JsonProperty("6")]
        public breakdownCostObject[] breakdownPaxPosition6 { get; set; }
        [JsonProperty("7")]
        public breakdownCostObject[] breakdownPaxPosition7 { get; set; }
    }

    //unnecesasry layer
    public class breakdownPaxPosition
    {
        [JsonProperty("0")]
        public breakdownCostObject breakdownCostObject0 { get; set; }
        [JsonProperty("1")]
        public breakdownCostObject breakdownCostObject1 { get; set; }
        [JsonProperty("2")]
        public breakdownCostObject breakdownCostObject2 { get; set; }
        [JsonProperty("3")]
        public breakdownCostObject breakdownCostObject3 { get; set; }
        [JsonProperty("4")]
        public breakdownCostObject breakdownCostObject4 { get; set; }
        [JsonProperty("5")]
        public breakdownCostObject breakdownCostObject5 { get; set; }
        [JsonProperty("6")]
        public breakdownCostObject breakdownCostObject6 { get; set; }
    }

    public class breakdownCostObject
    {
            [JsonProperty("segmentFee")]
            public double segmentFee { get; set; }
            [JsonProperty("subTotal")]
            public double subTotal { get; set; }
            [JsonProperty("taxes")]
            public Taxes taxes { get; set; }
            [JsonProperty("total")]
            public double total { get; set; }
    }

    public class MO
    {
        //[JsonProperty("mo")]
        //public paxPosition[] paxPosition { get; set; }

        //[JsonProperty("0")] nope
        //public double paxPosition0 { get; set; }

        //[JsonProperty("1")] nope
        //public Array[] paxPosition1 { get; set; }

        [JsonProperty("0")]
        public List<double> paxPosition0 { get; set; }
        [JsonProperty("1")]
        public List<double> paxPosition1 { get; set; }
        [JsonProperty("2")]
        public List<double> paxPosition2 { get; set; }
        [JsonProperty("3")]
        public List<double> paxPosition3 { get; set; }
        [JsonProperty("4")]
        public List<double> paxPosition4 { get; set; }
        [JsonProperty("5")]
        public List<double> paxPosition5 { get; set; }
        [JsonProperty("6")]
        public List<double> paxPosition6 { get; set; }
        [JsonProperty("7")]
        public List<double> paxPosition7 { get; set; }
    }

    public class preFlightFinance
    {
        [JsonProperty("paxPos")]
        public int paxPos { get; set; }
        [JsonProperty("firstName")]
        public string firstName { get; set; }
        [JsonProperty("lastName")]
        public string lastName { get; set; }
        [JsonProperty("receiptPrice")]
        public double receiptPrice { get; set; }
        [JsonProperty("finalPrice")]
        public double finalPrice { get; set; }
        [JsonProperty("refundAmount")]
        public double refundAmount { get; set; }
        [JsonProperty("flytzeRevenue")]
        public double flytzeRevenue { get; set; }


    }

        //instead of making paxPosition0-max#ofPlaneSeatsEver I need to be able to have general array name that accepts i and can iterate
        //public class paxPosition0
        //{
        //    public double moPrices0 { get; set; }
        //}
        //public class paxPosition1
        //{
        //    public double moPrices1 { get; set; }
        //}
        //public class MO
        //{
        //    [JsonProperty("mo")]
        //    public List<double> mo { get; set; }
        //}
    }
