﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SimplePortal.Objects
{
    public class nullActual
    {
        [JsonProperty("actual")]
        public object actual { get; set; }
    }
}