﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//This object is used to test Winner Cost pages without needed the wall
namespace SimplePortal.Objects
{
    public class TestWinnerCosts
    {
        public string Operator { get; set; }
        public int FeeVariableSegment { get; set; }
        public int FeeFixedSegment { get; set; }
        public bool SegmentVariableBool { get; set; }
        public double AirportConcessionRate { get; set; }
        public int FeeFixedLanding { get; set; }
        public int FeeVariableLanding { get; set; }
        public bool LandingVariableBool { get; set; }
        public int FeeSecurity { get; set; }
        public int FeeRamp { get; set; }
        public int FeeHandling { get; set; }
        public int FeeParking { get; set; }
        public bool TaxiMinutesBool { get; set; }
        public int TaxiMinutes { get; set; }
        public double TaxiOffsetRate { get; set; }
        public int PlaneHourlyRate { get; set; }
        public int CostDailyCrew { get; set; }
        public int FuelSurchargeRate { get; set; }
        public int FeeCallout { get; set; }
        public int FeeHangar { get; set; }
        public int FeeLav { get; set; }
        public int FeeGPU { get; set; }
        public int Fee2ndLanding { get; set; }
        public int FeeOvernight { get; set; }
        public int ExpensesNightlyCrew { get; set; }



        public TestWinnerCosts (string FBOAirport)
        {
            Operator = FBOAirport;
        }
    }
}