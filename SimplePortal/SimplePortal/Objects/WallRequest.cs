﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplePortal.Objects
{
    /// <summary>
    /// this is the wall request
    /// </summary>
    public class WallRequest
    {

        public string function { get; set; }
        public object functionParams { get; set; }
        public string pepper { get; set; }
        public string saltSecure { get; set; }
        public string sentTime { get; set; }
        public string token { get; set; }
        public string username { get; set; }


    }

    public class @params
    {

    }
}