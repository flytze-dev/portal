﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplePortal.Objects
{
    public class sendUpdate
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("updates")]
        //public Dictionary<string, long> updates { get; set; }
        public object updates { get; set; }

        //public sendUpdate(string updateId)
        //{
        //    id = updateId;
        //}
    }
}