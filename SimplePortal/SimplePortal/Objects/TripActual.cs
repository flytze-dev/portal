﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SimplePortal.Objects
{
    //this structure is based on the functionResponse from Postman calling readTrips
    public class TripActual
    {
        [JsonProperty("arrival")]
        public ActualArrival arrival { get; set; }
        [JsonProperty("departure")]
        public ActualDeparture departure { get; set; }
        [JsonProperty("flightDistance")]
        public ActualFlightDist flightDistance { get; set; }
        [JsonProperty("flightTime")]
        public ActualFlightTime flightTime { get; set; }
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("mo")]
        public MO mo { get; set; }
        [JsonProperty("actualCost")]
        public ActualTotalCost actualCost { get; set; }

        //[JsonProperty("actualCost")]
        //public ActualCost actualCost { get; set; }
        //[JsonProperty("arrivalTime")]
        //public object arrivalTime { get; set; }
        //[JsonProperty("departureTime")]
        //public object departureTime { get; set; }
        //[JsonProperty("flightDistance")]
        //public object flightDistance { get; set; }
        //[JsonProperty("flightTime")]
        //public object flightTime { get; set; }
    }

    public class ActualArrival
    {
        [JsonProperty("airportCode")]
        public string airportCode { get; set; }
        [JsonProperty("airportImage")]
        public string airportImage { get; set; }
        [JsonProperty("city")]
        public string city { get; set; }
        [JsonProperty("estimated")]
        public string estimatedArrival { get; set; }

        [JsonProperty("actual")]
        public string actualArrival { get; set; }
        [JsonProperty("estimatedEpoc")]
        public string estimatedEpoc { get; set; }
        [JsonProperty("stateName")]
        public string stateName { get; set; }
    }

    public class ActualDeparture
    {
        [JsonProperty("airportCode")]
        public string airportCode { get; set; }
        [JsonProperty("airportImage")]
        public string airportImage { get; set; }
        [JsonProperty("city")]
        public string city { get; set; }
        [JsonProperty("estimated")]
        public string estimatedDeparture { get; set; }

        [JsonProperty("actual")]
        public string actualDeparture { get; set; }
        [JsonProperty("estimatedEpoc")]
        public string estimatedEpoc { get; set; }
        [JsonProperty("stateName")]
        public string stateName { get; set; }
    }

    public class ActualFlightDist
    {
        [JsonProperty("actual")]
        public double actual { get; set; }
        [JsonProperty("estimated")]
        public double estimated { get; set; }
    }

    public class ActualFlightTime
    {
        [JsonProperty("actual")]
        public double actual { get; set; }
        [JsonProperty("estimated")]
        public double estimated { get; set; }
    }

    //public class ActualTotalCost
    //{
    //    [JsonProperty("airportConcession")]
    //    public double airportConcession { get; set; }
    //    [JsonProperty("baseRate")]
    //    public double baseRate { get; set; }
    //    [JsonProperty("dailyCrewCost")]
    //    public double dailyCrewCost { get; set; }
    //    [JsonProperty("flightCharges")]
    //    public double flightCharges { get; set; }
    //    [JsonProperty("fuelSurcharge")]
    //    public double fuelSurcharge { get; set; }
    //    [JsonProperty("miscFee")]
    //    public long miscFee { get; set; }
    //    [JsonProperty("segmentFee")]
    //    public long segmentFee { get; set; }
    //    [JsonProperty("subTotal")]
    //    public double subTotal { get; set; }
    //    //old tax structure, 5/23 started new
    //    //[JsonProperty("taxes")]
    //    //public double taxes { get; set; }
    //    [JsonProperty("taxes")]
    //    public Taxes taxes { get; set; }
    //    [JsonProperty("total")]
    //    public double total { get; set; }
    //}
    public class ActualTotalCost
    {
        [JsonProperty("baseRate")]
        public double baseRate { get; set; }
        [JsonProperty("taxes")]
        public Taxes taxes { get; set; }
        [JsonProperty("total")]
        public double total { get; set; }

    }

    //public class Taxes
    //{
    //    [JsonProperty("fetRate")]
    //    public double fetRate { get; set; }
    //    [JsonProperty("stateSalesTax")]
    //    public double stateSalesTax { get; set; }        
    //    [JsonProperty("total")]
    //    public double total { get; set; }
    //    [JsonProperty("wrpaTax")]
    //    public double wrpaTax { get; set; }
    //}
    public class Taxes
    {
        [JsonProperty("fetRate")]
        public double fetRate { get; set; }
        [JsonProperty("stateSalesTax")]
        public int stateSalesTax { get; set; }
        [JsonProperty("total")]
        public double total { get; set; }
        [JsonProperty("wrpaTax")]
        public double wrpaTax { get; set; }


    }
    public class BaseRate
    {
        //I don't think this is used
        [JsonProperty("baseRate")]
        public long baseRate { get; set; }
        [JsonProperty("segmentFee")]
        public long segmentFee { get; set; }
        [JsonProperty("taxes")]
        public double taxes { get; set; }
        [JsonProperty("total")]
        public double total { get; set; }
    }

    //public class ActualCost
    //{
    //    [JsonProperty("actualCost")]
    //    public TotalCost totalCost { get; set; }
    //}
    //public class OnlyActualCost { 
    //    [JsonProperty("actualCost")]
    //    public double total { get; set; }

    //    [JsonProperty("totalCost")]
    //    public Dictionary<string, object> totalCost { get; set; }
    //}

    public class UpdateTripActuals
    {
        [JsonProperty("id")]
        public string id { get; set; }
        //[JsonProperty("totalCost")]
        //public TotalCost totalCost { get; set; }
        [JsonProperty("actualCost")]
        public ActualTotalCost actualCost { get; set; }
    }

    public class UpdateTripActuals2
    {
        [JsonProperty("id")]
        public string id { get; set; }
        //[JsonProperty("totalCost")]
        //public TotalCost totalCost { get; set; }
        [JsonProperty("actualCost")]
        public double actualCost { get; set; }
    }

    public class UpdateTrip
    {
        [JsonProperty("actualCost")]
        public ActualTotalCost actualCost { get; set; }
        [JsonProperty("arrival")]
        public ActualString arrival { get; set; }
        [JsonProperty("departure")]
        public ActualString departure { get; set; }
        [JsonProperty("flightDistance")]
        public ActualDouble flightDistance { get; set; }
        [JsonProperty("flightTime")]
        public ActualDouble flightTime { get; set; }

        //        [JsonProperty("flightTime")]
        //        public ActualDouble flightTime { get; set; }
        [JsonProperty("id")]
        public string id { get; set; }
    }

    public class ActualString
    {
        [JsonProperty("actual")]
        public string actual { get; set; }
    }

    public class ActualDouble
    {
        [JsonProperty("actual")]
        public double actual { get; set; }
    }
}