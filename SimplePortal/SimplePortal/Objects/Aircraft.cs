﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SimplePortal.Objects
{
    public class Aircraft
    {
        //[JsonProperty("costPerHour")]
        //public string CostPerHour { get; set; }

        //[JsonProperty("cruiseSpeed")]
        //public string CruiseSpeed { get; set; }

        [JsonProperty("engine")]
        public string engine { get; set; }

        [JsonProperty("fboId")]
        public string fboId { get; set; }

        [JsonProperty("fboOwner")]
        public string fboOwner { get; set; }

        [JsonProperty("features")]
        //public object features { get; set; }
        public List<string> features { get; set; }

        [JsonProperty("fees")]
        public AircraftFees fees { get; set; }

        [JsonProperty("fleetId")]
        public string fleetId { get; set; }

        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("images")]
        //public string[] images { get; set; }
        public List<string> images { get; set; }

        [JsonProperty("maxCruiseAltitude")]
        public long maxCruiseAltitude { get; set; }

        [JsonProperty("maxRange")]
        public long maxRange { get; set; }

        [JsonProperty("runwayLength")]
        public long runwayLength { get; set; }

        [JsonProperty("seatCount")]
        public int seatCount { get; set; }

        [JsonProperty("speed")]
        public long speed { get; set; }

        [JsonProperty("tailNumber")]
        public string tailNumber { get; set; }
        //public List<string> images { get; set; }

        [JsonProperty("type")]
        public string type { get; set; }
    }

    public class AircraftFees
    {
        [JsonProperty("factors")]
        public List<double> factors { get; set; }
        [JsonProperty("hourlyRate")]
        public long hourlyRate { get; set; }
     }

    public class RequestById
    {
        public string id { get; set; }
    }

    public class UpdateAircraft
    {
        public string id { get; set; }
        public object update { get; set; }

        //fboId
        //    Images
        //    maxRange
        //    maxCruiseAltitude
            
        //    if "" not in requestParams or requestParams["fboOwner"] == "":
        //    raise ValueError('Plane fboOwner field is required')
        //if "" not in requestParams or requestParams["fboId"] == "":
        //    raise ValueError('Plane fboId field is required')
        //if "" not in requestParams or requestParams["Images"] == "":
        //    raise ValueError('Plane Images field is required')
        //if "" not in requestParams or requestParams["maxRange"] == "":
        //    raise ValueError('Plane maxRange field is required')
        //if "" not in requestParams or requestParams["maxCruiseAltitude"] == "":
        //    raise ValueError('Plane maxCruiseAltitude field is required')
        //if "type" not in requestParams or requestParams["type"] == "":
        //    raise ValueError('Plane type field is required')
        //if "seatCount" not in requestParams or requestParams["seatCount"] == "":
        //    raise ValueError('Plane seatCount field is required')
        //if "engine" not in requestParams or requestParams["engine"] == "":
        //    raise ValueError('Plane engine field is required')
        //if "speed" not in requestParams or requestParams["speed"] == "":
        //    raise ValueError('Plane speed field is required')
        //if "runwayLength" not in requestParams or requestParams["runwayLength"] == "":
        //    raise ValueError('Plane runwayLength field is required')
        //if "features" not in requestParams or requestParams["features"] == "":
        //    raise ValueError('Plane features field is required')
        //if "fees" not in requestParams or requestParams["fees"] == "":
        //    raise ValueError('Plane fees field is required')

    }
}