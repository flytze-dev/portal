﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SimplePortal.Objects
{
    public class AccountCollection
    {
        [JsonProperty("_id")]
        public string id { get; set; }

        [JsonProperty("accessGroup")]
        public string accessGroup { get; set; }

        [JsonProperty("createdDateTime")]
        public string createdDateTime { get; set; }

        [JsonProperty("dateOfBirth")]
        public string dateOfBirth { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("firstName")]
        public string firstName { get; set; }

        [JsonProperty("gender")]
        public string gender { get; set; }

//        [JsonProperty("homeAirportId")]
//        public string homeAirport { get; set; }

        [JsonProperty("iosToken")]
        public string iosToken { get; set; }

        [JsonProperty("isActive")]
        public string isActive { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("lastUpdatedBy")]
        public string lastUpdatedBy { get; set; }

        [JsonProperty("lastUpdatedDateTime")]
        public string lastUpdatedDateTime { get; set; }

        [JsonProperty("lockout")]
        public Lockout lockout { get; set; }

        [JsonProperty("middleName")]
        public string middleName { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }

        [JsonProperty("phoneNumber")]
        public long phoneNumber { get; set; }

        [JsonProperty("privilege")]
        public int privilege { get; set; }

        //        [JsonProperty("public")]
        //        public Public Public { get; set; }

        //        [JsonProperty("type")]
        //        public string AccountType { get; set; }
        [JsonProperty("title")]
        public string title{ get; set; }

        //        [JsonProperty("tripHistory")]
        //        public TripHistory TripHistory { get; set; }
        [JsonProperty("username")]
        public string username { get; set; }

        [JsonProperty("wallToken")]
        public string wallToken { get; set; }

        [JsonProperty("weight")]
        public string weight { get; set; }

    }

    public class TripHistory
    {
        //how to make it so anything can be entered?
    }

    public class Public
    {
        //how to make it so anything can be entered?
    }

    public class Lockout
    {
        [JsonProperty("attempts")]
        public long Attempts { get; set; }

        [JsonProperty("resetAt")]
        public long ResetAt { get; set; }
    }

    public class UpdateAccessGroup
    {
        [JsonProperty("accessGroup")]
        public string accessGroup { get; set; }

        [JsonProperty("username")]
        public string username { get; set; }
    }

    public class UpdateProfile
    {
        [JsonProperty("firstName")]
        public string firstName { get; set; }

        [JsonProperty("lastName")]
        public string lastName { get; set; }

        [JsonProperty("phoneNumber")]
        public string phoneNumber { get; set; }

        [JsonProperty("username")]
        public string username { get; set; }        
    }

    public class ViewUserProfile
    {
        [JsonProperty("accessGroup")]
        public string accessGroup { get; set; }

        [JsonProperty("dateOfBirth")]
        public string dateOfBirth { get; set; }

        [JsonProperty("firstName")]
        public string firstName { get; set; }

        [JsonProperty("gender")]
        public string gender { get; set; }

        [JsonProperty("lastName")]
        public string lastName { get; set; }

        [JsonProperty("middleName")]
        public string middleName { get; set; }

        [JsonProperty("paypalCardId")]
        public string userpaypalCardIdname { get; set; }

        [JsonProperty("phoneNumber")]
        public string phoneNumber { get; set; }

        [JsonProperty("username")]
        public string username { get; set; }

        [JsonProperty("weight")]
        public string weight { get; set; }

        [JsonProperty("winnerAccount")]
        public string winnerAccount { get; set; }
    }

public class RequestByUsername
    {
        public string username { get; set; }
    }


}