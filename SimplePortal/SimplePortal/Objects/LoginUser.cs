﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace SimplePortal.Objects
{

    public class LoginUserParameters
    {
        [DataMember(EmitDefaultValue = false)]
        [JsonProperty("accessGroup")]
        public string accessGroup { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty("token")]
        public string token { get; set; }

        [JsonProperty("username")]
        public string username { get; set; }

    }


    //public class LoginUserParameters
    //{
    //    [JsonProperty("password")]
    //    public string password { get; set; }

    //    [JsonProperty("username")]
    //    public string username { get; set; }
    //}


    //public class LoginUserResponse { 

    //    [JsonProperty(PropertyName = "accessGroup")]
    //    public string AccessGroup { get; set; }
    //}
}