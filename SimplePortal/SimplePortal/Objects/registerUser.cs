﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SimplePortal.Objects
{
    public class registerNewUser
    {
        [JsonProperty("accessGroup")]
        public string accessGroup { get; set; }
        [JsonProperty("dateOfBirth")]
        public string dateOfBirth { get; set; }
        [JsonProperty("firstName")]
        public string firstName { get; set; }
        [JsonProperty("gender")]
        public string gender { get; set; }       
        [JsonProperty("iosToken")]
        public string iosToken { get; set; }
        [JsonProperty("lastName")]
        public string lastName { get; set; }
        [JsonProperty("middleName")]
        public string middleName { get; set; }
        [JsonProperty("password")]
        public string password { get; set; }
        [JsonProperty("phoneNumber")]
        public long phoneNumber { get; set; }
        [JsonProperty("type")]
        public string type { get; set; }
        [JsonProperty("username")]
        //email
        public string username { get; set; }        
        [JsonProperty("weight")]
        public long weight { get; set; }

    }
}