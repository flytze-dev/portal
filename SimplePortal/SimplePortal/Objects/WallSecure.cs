﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SimplePortal.Objects
{
    /// <summary>
    /// this is the secure wall request
    /// </summary>
    //ataContract]
    public class WallRequestSecure
    {       
        public string username { get; set; }
        public string sentTime { get; set; }
        public string function { get; set; }
        public object functionParams { get; set; }
        public string hash { get; set; }
        public string salt { get; set; }

        //public string function { get; set; }
        //public object functionParams { get; set; }
        //public string hash { get; set; }
        //public string salt { get; set; }
        //public string sentTime { get; set; }
        //public string username { get; set; }
    }
}