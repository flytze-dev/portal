﻿
//FBO Tasks - FBO Operator Section of The App
//1. Splash screen – an opening screen with the App logo welcoming the user.This
//loads the first time the App is opened.If the App was left on a screen in the App,
//then this welcome screen is not loaded

//2. App should have analytics data (similar to Google Analytics, clicky, etc.) so the
//Admins can review at any time

//3. Record all searches with a view to give us an idea of where people want to go.
//Record incorrect searches as well

//4. FBO Operator sign in screen
//o Sign in is required
//o FBO Operator can create an account using an email address, which will be
//o If they already have an account, The FBO Operator can request sign in help
//o All FBO accounts must be approved/verified by an Admin; once verified, then
//o Upon account creation, send an email to the email addresses used that the
//used as their username.Add a field for FBO Name, FBO Representative
//Name, Phone Number, Address of FBO.Request password twice using 
//normal password protocols.Passwords must be at least 8 characters
//by using a forgot password function where an email address is requested and
//instructions sent to that email address on how to sign in. Typically a link to
//reset the password to the account.
//the FBO Operator can access the system.
//account is under review and they will be contacted shortly.
//o Notify the Admin App that a new operator has registered

//5. Once the account is approved/verified by an Admin, send an email to the email
//address in the application that they are approved and give them a link to sign in 
//to their account or direct them to the App to sign in.

//6. FBO Operator has the ability to:
//a.Send Customer notifications
//b. Read Customer communications
//c. Update Inventory
//i.Change any of the fields
//ii. Track changes; i.e.which operator changed anything
//d.View Bookings
//e.Assign Aircraft
//i.Registration
//ii. Aircraft Name
//iii.Names of Pilots
//f. Access Accounting System
//g. Access Aircraft Maintenance System
//i.Review aircraft maintenance history
//ii.Update aircraft maintenance history

//7. FBO Operator enters and/or uploads flight schedule information and availability
//a. Flight Number
//b.From
//c. To
//d. Dates
//e. Aircraft Type
//f.Seats Available – if uploading/updating availability
//g.Aircraft Capacity
//h.Pricing per seat

//8. FBO saves information (Pricing, schedules, availability) which is now available for 
//use as appropriate to the customer facing system and Admin console

//9. Once a booking is made on the customer facing App, the FBO Operator is sent a
//notification that a new booking is available; so the FBO Operator should be able
//to receive that notification on their version of the App

//10. FBO Operator can now contact customer as they see fit using the information in 
//the booking data
//a.Ability to make a call
//b.Ability to email customer
//c.Ability to text customer

//11. App should ensure that all the data is stored in secured (sign in required)
//databases so that the FBO(access level required) and an Admin(access level required) can view the appropriate data.