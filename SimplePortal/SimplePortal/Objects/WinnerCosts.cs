﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;


namespace SimplePortal.Objects
{
    ////works for posting updateFbo!!!!!!!!!!!!!!
    public class WinnerCosts
    {
        [JsonProperty("accessGroup")]
        public string accessGroup { get; set; }
        [JsonProperty("email")]
        public string email { get; set; }
        [JsonProperty("fees")]
        public Fees fees { get; set; }
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("isActivated")]
        public bool isActivated { get; set; }
        [JsonProperty("isValidated")]
        public bool isValidated { get; set; }
        [JsonProperty("maxDistance")]
        public long maxDistance { get; set; }
        [JsonProperty("password")]
        public string password { get; set; }
        [JsonProperty("phone")]
        public long phone { get; set; }
        [JsonProperty("privilege")]
        public long privilege { get; set; }
        [JsonProperty("title")]
        public string title { get; set; }
        [JsonProperty("type")]
        public string type { get; set; }
    }

    public class Fees
    {
        [JsonProperty("airportConcession")]
        // public double airportConcession { get; set; }
        public double airportConcession { get; set; }

        [JsonProperty("custom")]
        //public Dictionary<string, object> custom { get; set; }
        public object custom { get; set; }

        [JsonProperty("dailyCrewCost")]
        public long dailyCrewCost { get; set; }

        [JsonProperty("factors")]
        public List<double> factors { get; set; }

        [JsonProperty("fuelSurchargeRate")]
        public long fuelSurchargeRate { get; set; }

        [JsonProperty("handling")]
        public long handling { get; set; }

        [JsonProperty("hourlyRate")]
        public long hourlyRate { get; set; }

        [JsonProperty("landing")]
        public landingFee landing { get; set; }

        [JsonProperty("parking")]
        public long parking { get; set; }

        [JsonProperty("ramp")]
        public long ramp { get; set; }

        [JsonProperty("security")]
        public long security { get; set; }

        [JsonProperty("segment")]
        public segmentFee segment { get; set; }

        [JsonProperty("taxiTime")]
        public taxiTime taxiTime { get; set; }

        //[JsonProperty("taxi")]
        //public taxiTime taxi { get; set; }
    }

    public class landingFee
    {
        [JsonProperty("fee")]
        public long fee { get; set; }
        [JsonProperty("isFixed")]
        public bool isFixed { get; set; }
    }
    public class segmentFee
    {
        [JsonProperty("fee")]
        public long fee { get; set; }
        [JsonProperty("isFixed")]
        public bool isFixed { get; set; }
    }
    public class taxiTime
    {
        [JsonProperty("minutes")]
        public long minutes { get; set; }
        [JsonProperty("offset")]
        public double offset { get; set; }
        [JsonProperty("useMinutes")]
        public bool useMinutes { get; set; }
    }
}













/////// <summary>
/////// all on one plane, never worked
/////// </summary>
////namespace SimplePortal.Objects
////{
////    public class WinnerCosts
////    {
////        [JsonProperty("accessGroup")]
////        public string accessGroup { get; set; }
////        [JsonProperty("airportConcession")]
////        public double airportConcession { get; set; }

////        [JsonProperty("dailyCrewCost")]
////        public long dailyCrewCost { get; set; }


////        [JsonProperty("email")]
////        public string email { get; set; }

////        //[JsonProperty("factors")]
////        //public List<Factor> factors { get; set; }
////        [JsonProperty("fee")]
////        public long fee { get; set; }
////        [JsonProperty("fees")]
////        public object fees { get; set; }

////        [JsonProperty("fuelSurchargeRate")]
////        public long fuelSurchargeRate { get; set; }

////        [JsonProperty("handling")]
////        public long handling { get; set; }

////        [JsonProperty("hourlyRate")]
////        public long hourlyRate { get; set; }


////        [JsonProperty("id")]
////        public string id { get; set; }
////        [JsonProperty("isActivated")]
////        public bool isActivated { get; set; }
////        [JsonProperty("isFixed")]
////        public bool isFixed { get; set; }
////        [JsonProperty("isValidated")]
////        public bool isValidated { get; set; }
////        [JsonProperty("landing")]
////        public object landing { get; set; }
////        [JsonProperty("maxDistance")]
////        public long maxDistance { get; set; }

////        [JsonProperty("minutes")]
////        public long minutes { get; set; }
////        [JsonProperty("offset")]
////        public double offset { get; set; }



////        [JsonProperty("parking")]
////        public long parking { get; set; }

////        [JsonProperty("password")]
////        public string password { get; set; }
////        [JsonProperty("phone")]
////        public long phone { get; set; }
////        [JsonProperty("privilege")]
////        public long privilege { get; set; }
////        [JsonProperty("ramp")]
////        public long ramp { get; set; }
////        [JsonProperty("security")]
////        public long security { get; set; }

////        [JsonProperty("segment")]
////        public object segment { get; set; }

////        [JsonProperty("taxi")]
////        public object taxi { get; set; }
////        [JsonProperty("title")]
////        public string title { get; set; }
////        [JsonProperty("type")]
////        public string type { get; set; }

////        [JsonProperty("useMinutes")]
////        public bool useMinutes { get; set; }
////    }
////}