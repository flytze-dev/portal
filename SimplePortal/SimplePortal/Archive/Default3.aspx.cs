﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Security;

namespace SimplePortal
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //WallTest.Visible = false;
            //ManageCosts.Visible = false;
            //ManageFactors.Visible = false;

            //var userName = HttpContext.Current.User.Identity.Name.ToString();
            //if (userName == "david@cyberprops.com")
            //{
            //    WallTest.Visible = true;
            //    ManageCosts.Visible = true;
            //    ManageFactors.Visible = true;

            //}
        }
        protected void LoginStatus1_LoggingOut(Object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label3.Text = "LoggingOut event. Don't go away now.";
        }
    }
}