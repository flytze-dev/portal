﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default3.aspx.cs" Inherits="SimplePortal._Default" %>
<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <link rel="stylesheet" href="~/Content/Fresh.css" />
    <meta charset="utf-8" />
    <webopt:bundlereference runat="server" path="~/Content/css" />    
    <title><%: Page.Title %>: Flytze Portal</title>

</head>

    <body>
<form id="FORM1" runat="server">

        <header>THis is the HEader:::

        <div class="TestDropdown nav-wrapper">
   <nav class="nav-menu">
      <ul class="clearfix">
         <li><a href="/">Home</a></li>
         <li><a href="/Contributors">Contributors</a>
         <ul class="sub-menu"> 
            <li><a href="/jordan">Michael Jordan</a></li> 
            <li><a href="/hawking">Stephen Hawking</a></li> 
         </ul> 
         </li>
         <li><a href="#!">Contact Us</a>
         <ul class="sub-menu"> 
            <li><a href="mailto:bugsupport@company.com">Report a Bug</a></li> 
            <li><a href="/support">Customer Support</a></li> 
         </ul> 
         </li>
      </ul>
   </nav>
</div>
    </header>


    <div class="Navigate" id="NavigateId"> 
 
            <div class="pull-right">
                <section id="login">
                    <asp:LoginView runat="server" ViewStateMode="Disabled">
                        <AnonymousTemplate>
                            <ul>
                                <li><a id="registerLink" runat="server" href="~/Account/Register.aspx">Register</a></li>
                                <li><a id="loginLink" runat="server" href="~/Account/Login.aspx">Log in</a></li>
                            </ul>
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            <p>
                                Hello, <a runat="server" class="username" href="~/Account/Manage.aspx" title="Manage your account">
                                    <asp:LoginName runat="server" CssClass="username" /></a>!
                                    <asp:LoginStatus id="LoginStatus1" runat="server" onloggingout="LoginStatus1_LoggingOut"></asp:LoginStatus>
<%--                                    <asp:LinkButton id="LoginOutLink" Text="Log Out" OnClick="LogOutLink_OnClick" runat="server" />--%>
<%--                                <asp:LoginStatus runat="server" LogoutAction="Redirect" LogoutText="Log off" LogoutPageUrl="~/Account/Login.aspx" OnLoggingOut="Unnamed_LoggingOut" />--%>
                            </p>
                        </LoggedInTemplate>
                    </asp:LoginView>
                </section>
            </div>
         </div>


        <div id="NavView" runat="server">
               <div class="Nav">
                    <ul class="pull-right" runat="server">
                        <li><a runat="server" id="DeveloperWallTest" href="../DeveloperArea/ExchangeRatesPage.aspx">Wall Test</a></li>
                        <li><a runat="server" id="AdminMaintainMO" href="../AdminArea/MOMaintenance.aspx">Maintain MO</a></li>
                        <li><a runat="server" id="AdminUserAdmin" href="../AdminArea/UserAdministration.aspx">Manage Users</a></li>
                        <li><a runat="server" id="WinnerFees" href="../WinnerArea/OperatorFeeMaint_4Col.aspx">Winner Costs</a></li>
                        <li><a runat="server" id="SecondFBOFees" href="../SecondFBOArea/SecondFBO.aspx">SecondFBO Costs</a></li>
                    </ul>
                </div>
            </div>

    <div class="jumbotron">
        <h1>ASP.NET<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        </h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;
            
            </a></p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">
            
            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
            </a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>
</form>
        </body>
    </html>