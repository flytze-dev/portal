﻿using SimplePortal.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SimplePortal.Objects
{
    public class FboGetAllResponse: OperationResult
    {
        public FBO[] fbos { get; set; }
    }
}