﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SimplePortal.Startup))]
namespace SimplePortal
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
