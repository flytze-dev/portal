﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/3Column.Master" AutoEventWireup="true" CodeBehind="ExchangeRatesPage.aspx.cs" Inherits="SimplePortal.DeveloperArea.ExchangeRatesPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Njumbotron" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="NLeftHead" runat="server">
    <p>
    Test Security (Salt, saltUser, Hash capabilities)
</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="NLeftParag" runat="server">
    new<br />
salt:<br />
    <asp:Label ID="lblSalt" runat="server" Text="Label"></asp:Label>
    <br />
    saltUser:<br />
    <asp:Label ID="lblsaltUser" runat="server" Text="Label"></asp:Label>
    <br />
    hash:<br />
    <asp:Label ID="lblHash" runat="server" Text="Label"></asp:Label>
    <br />
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
    <br />
    <br />
    Page loads and, via ExchangeRates.cs,the API is called and ExchangeRates object is created.&nbsp;
    <br />
    The labels are populated with properties from ExchangeRates object.<br />
    <br />
    When you click the button the textbox follows the same process and is populated.
    <br />
    <br />
    Disclaimer:<br />
    <asp:Label ID="lblDisclaimer" runat="server" Text="Label"></asp:Label>
    <br />
    <br />
    Base:<br />
    <asp:Label ID="lblBase" runat="server" Text="Label"></asp:Label>
    <br />
    <br />
    Rate:<br />
    <asp:Label ID="lblBase0" runat="server" Text="Label"></asp:Label>
    <br />
    <br />
    Timestamp:<br />
    <asp:Label ID="lblBase1" runat="server" Text="Label"></asp:Label>
    <br />
    <br />
    License:<br />
    <asp:Label ID="lblBase2" runat="server" Text="Label"></asp:Label>
    <br />
    <br />
    Disclaimer:<br />
    <asp:TextBox ID="txtUrl" runat="server" Height="125px" TextMode="MultiLine" Width="551px"></asp:TextBox>
    <br />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NMidHead" runat="server">
    Wall Test
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NMidParag" runat="server">
    <p>
        wall info&nbsp;</p>
    <p>
        <asp:Label ID="function" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:Label ID="params" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:Label ID="pepper" runat="server" Text="Label"></asp:Label>
        ...</p>
    <p>
        <asp:Label ID="txtinfo" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:Label ID="sentTime" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:Label ID="token" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:Label ID="username" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        toEncode:&nbsp;&nbsp;
        <asp:Label ID="toEncode" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        wallSecure.hash&nbsp;&nbsp; <asp:Label ID="hashedRequest" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NRightHead" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="NRightParag" runat="server">
</asp:Content>
