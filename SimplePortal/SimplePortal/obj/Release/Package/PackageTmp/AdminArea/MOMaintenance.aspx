﻿<%@ Page Title="Maintain MO" Language="C#" MasterPageFile="../MasterPages/Portal.Master" AutoEventWireup="true" CodeBehind="MOMaintenance.aspx.cs" Inherits="SimplePortal.AdminArea.MOMaintenance" %>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Manage Monetization Optimization Figures
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
<div class="outter">
    <div class="col-md-12">
        <div class="LargeFont">

        <a id="show" class="LargeFont" href="javascript:expand();">Expand</a>
        <div id="hide" style="display: none" >
            <a class="LargeFont" href="javascript:collapse();">Collapse</a>

        <div class="contentWidePanel">
            <div class="LargeFont">
                Description of Factor Preferences
            </div>
            <div class="SmallFont">                
<%--        If Quote calls for Factor outside range of set Factors, then the lastest factor is used <br />
                For example: <br />
                If 10 factors are set and a plane has 12 seats full, then the Factors for the 11th and 12th seats will be the same as the 10th seat.<br />
                <br />--%>
                The quote formula will use the most specific Factors for a given trip.<br />
                For example: <br />
                If a specific plane has it's Factors set, then those Factors will be used. <br />
                If the plane taking the trip doesn't have it's Factors set, then the next option is to use the Operator's Factors. <br />
                If the Operator doesn't have Factors set, then the default Factors will be used. 
            </div>
       </div>


        </div>

            </div>


    </div>

<div class="col-md-4 BorderBuffer">
    <div class="ContentContainer">
<h3>Update Default Factor </h3>
    <div class = "LargeFont">
        Editing these fields updates the upcharge factor for the Pax position for all planes 
    </div>
        <div class ="SmallFont">            
            &nbsp;<table id="TableEditDefault">
                <tr class="TableTitles">
                    <td>PAX</td>
                    <td class="auto-style1">Factor</td>
                </tr>      
<%--                <tr>
                    <td>Seat 1:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax1" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax1" runat="server" Visible="False"></asp:TextBox></td>
                </tr> --%>               
                <tr>
                    <td>Seat 2:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax2" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax2" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 3:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax3" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax3" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 4:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax4" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax4" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 5:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax5" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax5" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 6:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax6" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax6" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 7:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax7" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax7" runat="server" Visible="False"></asp:TextBox></td>
                </tr>         
                <tr>
                    <td>Seat 8:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax8" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax8" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Seat 9:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax9" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax9" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Seat 10:</td>
                    <td class="auto-style1"><asp:Label ID="lblAllpax10" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtAllPax10" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
            </table>
        </div>
            <div >
        <p>
        <asp:Button ID="btnEditDefaultMO" runat="server" OnClick="btnEditDefaultMO_Click" Text="Edit Default Factors" CssClass="button" Width="182px" />                   
        <asp:Button ID="btnTestDefaultMO" runat="server" OnClick="btnTestDefaultMO_Click" Text="Confirm Default Factors" Visible="false" CssClass="button" Width="194px" />
            
        
        </p>
    </div>
</div>
</div>

<div class ="col-md-4" >
    <div class="ContentContainer">
<h3>Update Factor for Operator</h3>
    <div class = "LargeFont">
        Editing below fields updates the upcharge factor for the Pax position for an entire Operator
    </div>
    <div class =" SmallFont">
    <p>
        Select Operator to Edit:
        <asp:Label ID="lblOperatorToEdit" runat="server" Text="Choose Operator"></asp:Label>
    </p>
    <p>
        <asp:DropDownList ID="ddlOperatorToEdit" runat="server" OnSelectedIndexChanged="ddlOperatorToEdit_SelectedIndexChanged" AutoPostBack="True" DataTextField="title" datakeynames="id">
            <asp:ListItem Value="Select">Select Operator</asp:ListItem>
            <asp:ListItem Value="Winner">Winner</asp:ListItem>
<%--            <asp:ListItem Value="Second">Second FBO</asp:ListItem>
            <asp:ListItem Value="Third">Third</asp:ListItem>--%>

        </asp:DropDownList>
    </p>    
        </div>
        <div class ="SmallFont">
            <table id="TableEditFBO" runat="server">
                <tr class="TableTitles">
                    <td>PAX</td>
                    <td class="auto-style1">Factor</td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>      
<%--                <tr>
                    <td>Seat 1:</td>
                    <td class="auto-style1">
                        <asp:Label ID="lblOpPax1" runat="server" Text="Label"></asp:Label>
                        <asp:TextBox ID="txtOpPax1" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>   --%>             
                <tr>
                    <td>Seat 2:</td>
                    <td class="auto-style1">
                        <asp:Label ID="lblOpPax2" runat="server" Text="Label"></asp:Label>
                        <asp:TextBox ID="txtOpPax2" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>                
                <tr>
                    <td>Seat 3:</td>
                    <td class="auto-style1"><asp:Label ID="lblOpPax3" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtOpPax3" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>                
                <tr>
                    <td>Seat 4:</td>
                    <td class="auto-style1"><asp:Label ID="lblOpPax4" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtOpPax4" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>                
                <tr>
                    <td>Seat 5:</td>
                    <td class="auto-style1"><asp:Label ID="lblOpPax5" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtOpPax5" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>                
                <tr>
                    <td>Seat 6:</td>
                    <td class="auto-style1"><asp:Label ID="lblOpPax6" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtOpPax6" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>                
                <tr>
                    <td>Seat 7:</td>
                    <td class="auto-style1"><asp:Label ID="lblOpPax7" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtOpPax7" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>         
                <tr>
                    <td>Seat 8:</td>
                    <td class="auto-style1"><asp:Label ID="lblOpPax8" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtOpPax8" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>
                <tr>
                    <td>Seat 9:</td>
                    <td class="auto-style1"><asp:Label ID="lblOpPax9" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtOpPax9" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>
                <tr>
                    <td>Seat 10:</td>
                    <td class="auto-style1"><asp:Label ID="lblOpPax10" runat="server" Text="Label"></asp:Label><asp:TextBox id="txtOpPax10" runat="server" Visible="False"></asp:TextBox></td>
                    <td class="auto-style2">&nbsp;</td>
                </tr>
            </table>
         </div>

    <p>
        <asp:Button ID="btnEditOperator" runat="server" OnClick="btnEditOps_Click" Text="Edit Factors for Operator" Visible="false" CssClass="button" Width="249px" />
        <asp:Button ID="btnConfirmOperator" runat="server" OnClick="btnConfirmOps_Click" Text="Confirm Factors for Operator" Visible="false" CssClass="button" Width="262px" />
    </p>

</div>
</div>

<div class ="col-md-4">
    <div class="ContentContainer">
<h3>Update Factor on Specific Plane</h3>
    <div class = "LargeFont">
        Editing these fields update the upcharge factor for the Pax position on selected plane</div>
    <div class =" SmallFont">
    <p>
        Select Operator owner of plane:
        <asp:Label ID="lblOperatorPlane" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
       <asp:DropDownList ID="ddlPlaneOperator" runat="server" OnSelectedIndexChanged="ddlPlaneOperator_SelectedIndexChanged" AutoPostBack="True" DataTextField="title" datakeynames="id">
            <asp:ListItem Value="Select">Select Operator</asp:ListItem>
            <asp:ListItem Value="Winner">Winner</asp:ListItem>
            <asp:ListItem Value="Second">Second FBO</asp:ListItem>
            <asp:ListItem Value="Third">FL FBO</asp:ListItem>
        </asp:DropDownList>
    </p>
    
        </div>
        <div class ="SmallFont">
    <p>
        Select Plane to Update:
        <asp:Label ID="lblPlaneToEdit" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:DropDownList ID="ddlSelectPlane" runat="server" OnSelectedIndexChanged="ddlSelectPlane_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Value="Select">Select Plane</asp:ListItem>
            <asp:ListItem Value="Winner">Prop Plane</asp:ListItem>
        </asp:DropDownList>
    </p>
        
            <table id="TableEditSpecificPlane" runat="server">
                <tr class="TableTitles">
                    <td>PAX</td>
                    <td class="auto-style1">Factor</td>
                </tr>      
<%--                <tr>
                    <td>Seat 1:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax1" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax1" runat="server" Visible="False"></asp:TextBox></td>
                </tr>   --%>             
                <tr>
                    <td>Seat 2:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax2" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax2" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 3:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax3" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax3" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 4:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax4" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax4" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 5:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax5" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax5" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 6:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax6" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax6" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 7:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax7" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax7" runat="server" Visible="False"></asp:TextBox></td>
                </tr>         
                <tr>
                    <td>Seat 8:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax8" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax8" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Seat 9:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax9" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax9" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Seat 10:</td>
                    <td class="auto-style1"><asp:Label ID="lblPlanePax10" runat="server" Text="Label"></asp:Label><asp:TextBox ID="txtPlanePax10" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
            </table>
 
        </div>
    <p>
        <asp:Button ID="btnEditPlane" runat="server" OnClick="btnEditPlane_Click" Text="Edit Factors for a Plane" Visible="false" CssClass="button" />                       
        <asp:Button ID="btnConfirmPlane" runat="server" OnClick="btnConfirmPlane_Click" Text="Confirm Factors for a Plane" CssClass="button" Visible="false" Width="247px" />
    </p>

</div>
</div>



<%-- don't know why but without something here like the "." then 
    border of bottom div extends all the way to top of page--%>

    . 
<%--    <div class="col-md-12">
        <div class="contentWidePanel">
            <h3>Coming SOon ... Test Output from Changes</h3>
            <table id="TableEditMisc3">
                <tr class="TableTitles">
                    <td>&nbsp;</td>
                    <td class="auto-style1">&nbsp;</td>
                </tr>      
            </table>--%>

<%--    <div class = "LargeFont">
        The cumulative results for each seat.
    </div>--%>
<%--    <div class ="SmallFont">
   Test MO-Factors on different quote prices before confirming changes to Factors.

        </div>
            <asp:Button ID="btnConfirmDefaultMO" class="button" runat="server" OnClick="btnConfirmDefaultMO_Click" Text="Confirm Edits" Visible="false" CssClass="button" Width="124px" />
        <br />
       </div>
    </div>

</div>--%>
</asp:Content>