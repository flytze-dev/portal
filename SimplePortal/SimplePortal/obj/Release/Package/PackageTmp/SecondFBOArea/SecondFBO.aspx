﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Portal.Master" AutoEventWireup="true" CodeBehind="SecondFBO.aspx.cs" Inherits="SimplePortal.SecondFBOArea.SecondFBO" %>

<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Placeholder For Next FBO Client
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="column1 ContentContainer col-md-3">
<h3>FBO</h3>
    <p>FBO fees</p>
</div>

<div class="column2 ContentContainer col-md-3">
<h3>Flight Operations</h3>
        <p>Ops fees</p>
</div>

<div class="column3 ContentContainer col-md-3">
<h3>Airport</h3>
    <p>Airport fees</p>
</div>

<div class="column4 ContentContainer col-md-3">
<h3>Misc</h3>
    <p>Misc fees</p>
</div>
</asp:Content>