﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using SimplePortal.Helpers;
using SimplePortal.Results;
using SimplePortal.Objects;

namespace SimplePortal.MasterPages
{
    public partial class Portal : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Users cannot see content unless authenticated properly
            //Page.Master.FindControl("MainContent").Visible = false;
            //btnNavToLogin.Visible = true;
            //User cannot see tabs unless authenticated properly
            this.Page.Master.FindControl("NavView").FindControl("AdminMaintainMO").Visible = false;
            this.Page.Master.FindControl("NavView").FindControl("AdminUserAdmin").Visible = false;
            this.Page.Master.FindControl("NavView").FindControl("PreFlightDetails").Visible = false;
            this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = false;
            this.Page.Master.FindControl("NavView").FindControl("WinnerActuals").Visible = false;
            //assume Anonymous until authenticated
            this.Page.Master.FindControl("HelloUser").Visible = false;
            this.Page.Master.FindControl("Anonymous").Visible = true;

            if (Session["LoggedInUserAccessGroup"] != null && Session["LoggedInUser"] != null)
            {
                var userAccessGroup = Session["LoggedInUserAccessGroup"];
                var loggedInUser = Session["LoggedInUser"];                

                //Hide the screen if Access Group doesn't match
                //stops people from changing url and accessing directly 
                //TODO: Make function that takes userAccessGroup and compares it to all acessGroups in db and returns what user should see
                if (userAccessGroup.ToString() == "winner" || userAccessGroup.ToString() == "flytzeAdmin")
                {
                    Page.Master.FindControl("MainContent").Visible = true;
                }

                if (userAccessGroup.ToString() == "unassigned" && this.Page.AppRelativeVirtualPath.ToString() == "~/Account/EditUserProfile.aspx")
                {
                    Page.Master.FindControl("MainContent").Visible = true;
                }
                //else if (userAccessGroup.ToString() == "flytzeAdmin")
                //{
                //    Page.Master.FindControl("MainContent").Visible = true;
                //}

                    //Change login status indicator link
                LinkButton lblHelloUser = Page.Master.FindControl("HelloUser").FindControl("lblHelloUser") as LinkButton;
                lblHelloUser.Text = loggedInUser.ToString();
                this.Page.Master.FindControl("HelloUser").Visible = true;
                this.Page.Master.FindControl("Anonymous").Visible = false;

                //Make appropriate tabs visible for accessGroups
                switch (userAccessGroup.ToString())
                {
                    case "flytzeAdmin":
                        this.Page.Master.FindControl("NavView").FindControl("AdminMaintainMO").Visible = true;
                        this.Page.Master.FindControl("NavView").FindControl("AdminUserAdmin").Visible = true;
                        this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = true;
                        this.Page.Master.FindControl("NavView").FindControl("WinnerActuals").Visible = true;
                        this.Page.Master.FindControl("NavView").FindControl("PreFlightDetails").Visible = true;
                        break;
                    case "winner":
                        this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = true;
                        this.Page.Master.FindControl("NavView").FindControl("WinnerActuals").Visible = true;
                        break;
                    //case "second@fbo.com":
                    case "secondFBO":
                        this.Page.Master.FindControl("NavView").FindControl("SecondFBOFees").Visible = true;
                        break;
                    default:
                        break;
                }
            }
            else if (this.Page.AppRelativeVirtualPath.ToString() == "~/Account/LoginUser.aspx" || this.Page.AppRelativeVirtualPath.ToString() == "~/Account/RegisterUser.aspx" || this.Page.AppRelativeVirtualPath.ToString() == "~/Account/EditUserProfile.aspx")
            {
                Page.Master.FindControl("MainContent").Visible = true;
            }
            else
            {
                //btnNavToLogin.Visible = true;
                Response.Redirect("~/Account/LoginUser.aspx");
            }

        }

            //#region  " User Group Tab Visibility "
            ////this.Page.Master.FindControl("NavView").FindControl("DeveloperWallTest").Visible = false;
            //this.Page.Master.FindControl("NavView").FindControl("AdminMaintainMO").Visible = false;
            //this.Page.Master.FindControl("NavView").FindControl("AdminUserAdmin").Visible = false;
            //this.Page.Master.FindControl("NavView").FindControl("PreFlightDetails").Visible = false;
            //this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = false;
            //this.Page.Master.FindControl("NavView").FindControl("WinnerActuals").Visible = false;

            //this.Page.Master.FindControl("HelloUser").Visible = false;
            //this.Page.Master.FindControl("Anonymous").Visible = true;



            //if (Session["LoggedInUser"] != null)
            //{
            //    var loggedInUser = Session["LoggedInUser"];
            //    var userAccessGroup = Session["LoggedInUserAccessGroup"];

            //    //Label lblHelloUser = Page.Master.FindControl("HelloUser").FindControl("lblHelloUser") as Label;
            //    //lblHelloUser.Text = loggedInUser.ToString();

            //    LinkButton lblHelloUser = Page.Master.FindControl("HelloUser").FindControl("lblHelloUser") as LinkButton;
            //    lblHelloUser.Text = loggedInUser.ToString();

            //    this.Page.Master.FindControl("HelloUser").Visible = true;
            //    this.Page.Master.FindControl("Anonymous").Visible = false;

            //    //switch (loggedInUser.ToString())
            //    if (Session["LoggedInUserAccessGroup"] != null)
            //    {
            //        switch (userAccessGroup.ToString())
            //        {
            //            case "admin@flytze.com":
            //            case "flytzeAdmin":

            //                this.Page.Master.FindControl("NavView").FindControl("AdminMaintainMO").Visible = true;
            //                this.Page.Master.FindControl("NavView").FindControl("AdminUserAdmin").Visible = true;
            //                this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = true;
            //                this.Page.Master.FindControl("NavView").FindControl("WinnerActuals").Visible = true;
            //                this.Page.Master.FindControl("NavView").FindControl("PreFlightDetails").Visible = true;
            //                break;
            //            case "winner@winneraviation.com":
            //            case "winner":
            //                this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = true;
            //                this.Page.Master.FindControl("NavView").FindControl("WinnerActuals").Visible = true;
            //                break;
            //            case "second@fbo.com":
            //            case "secondFBO":
            //                this.Page.Master.FindControl("NavView").FindControl("SecondFBOFees").Visible = true;
            //                break;
            //            default:
            //                break;
            //        }
            //    }
            //}
            //#endregion                

        protected void LogOut_LoggingOut(Object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Session["LoggedInUser"] = null;
            Session["LoggedInUserAccessGroup"] = null;
            Session["LoggedInUserToken"] = null;
            Response.Redirect("~/Account/LoginUser.aspx");
        }

        protected void link_EditProfile(object sender, EventArgs e)
        {
            //Response.Redirect("~/Account/EditProfile.aspx");
            Response.Redirect("~/Account/EditUserProfile.aspx");
        }
    }
}