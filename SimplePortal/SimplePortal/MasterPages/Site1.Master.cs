﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SimplePortal.MasterPages
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            btnAdmin.Visible = false;
            var userName = HttpContext.Current.User.Identity.Name.ToString();
            lblUser.Text = userName.ToString();
            if (userName == "david@cyberprops.com")
            {
                btnAdmin.Visible = true;
            }

        }
    }
}