﻿<%@ Page Title="Trip Actuals" Language="C#" MasterPageFile="~/MasterPages/Portal.Master" AutoEventWireup="true" CodeBehind="WinnerActuals.aspx.cs" Inherits="SimplePortal.WinnerArea.WinnerActuals" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle" runat="server">
    Update Actual Post-Flight Figures  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="outter">
     <div class="col-md-12 BorderBuffer">  
        <div class="contentWidePanel">

            <div class="LargeFont">
                <h3>Update Past Trips</h3>
            </div>
            <table id="UpdateTripActuals">    
                </table>

<%--            <div style="width: 100%; height: 400px; overflow: scroll">--%>
                <p>
                    <asp:Label ID="lblNoTrips" runat="server" CssClass="LargeFont" Text="Label" Visible="False"></asp:Label>
        <asp:GridView ID="gvSelectTripSubmitActuals" runat="server" OnSelectedIndexChanged="gvSelectTripSubmitActuals_SelectedIndexChanged" CellPadding="4" ForeColor="#333333" datakeynames="id" GridLines="None" AutoGenerateColumns="False" AllowPaging="True">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" ButtonType="Button" SelectText="Update Trip" ControlStyle-CssClass="button" >
<ControlStyle CssClass="button"></ControlStyle>
                </asp:CommandField>
                <asp:BoundField datafield="id" headertext ="Trip ID"/>
                <asp:BoundField datafield="departure.airportCode" headertext ="Departure Airport"/>
                <asp:BoundField DataField="departure.estimated" HeaderText="Estimated Departure" />

                <asp:BoundField datafield="arrival.airportCode" headertext ="Arrival Airport"/>
                <asp:BoundField DataField="arrival.estimated" HeaderText="Estimated Arrival" />
                
<%--We calc time from dist and speed to no need to put actual dist if we get Operator to input actual time.
                    <asp:BoundField DataField="flightDistance.estimated" HeaderText="Estimated Flight Distance" />--%>
                <asp:BoundField DataField="flightTime.estimated" HeaderText="Estimated Flight Time" />

                <asp:BoundField DataField="totalCost.baseRate" HeaderText="Quoted Base Rate" />
                <asp:BoundField DataField="totalCost.total" HeaderText="Quoted Total" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />           
        </asp:GridView>


                    <asp:Label ID="lblUpdateTripActual" runat="server" Text="Label" Visible="False" CssClass="LargeFont"></asp:Label>
                </p>
               
            <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Color="red" runat="server" ControlToValidate="txtBaseRate" ErrorMessage="Actual Base Rate (Subtotal) amount is required."></asp:RequiredFieldValidator>
            <br />


 <table id="TableActualTripInfo" runat="server">    
    <tr class="TableTitles">
        <td class="LargeFont">Enter Validated Trip Information</td>
    </tr> 
        <tr class="largeFont">
        <td>Trip Variable</td>
        <td >Actual Value</td>
    </tr> 
    <tr>
        <td>Departure Time:</td>
        <td class="auto-style1">
            <asp:TextBox ID="txtActualDepartureTime" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="True" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Arrival Time:</td>
        <td class="auto-style1">
        <asp:TextBox ID="txtActualArrivalTime" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="True" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Flight Distance:</td>
        <td class="auto-style1">
        <asp:TextBox ID="txtActualFlightDistance" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="True" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Flight Duration (calculate instead?):</td>
        <td class="auto-style1">
        <asp:TextBox ID="txtActualFlightDuration" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="True" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
<%--    <tr>
        <td>Airport Concession:</td>
        <td class="auto-style1">
        <asp:TextBox ID="txtActualAirportConcession" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="True" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>--%>
    <tr>
        <td>Base Rate (subtotal):</td>
        <td class="auto-style1">
        <asp:TextBox ID="txtBaseRate" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="True" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
        
    </tr>
<%--    <tr>
        <td>Segment Fee:</td>
        <td class="auto-style1">
        <asp:TextBox ID="txtSegmentFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>--%>
    <tr>
        <td>Total Taxes:</td>
        <td class="auto-style1">
        <asp:TextBox ID="txtActualTotalTaxes" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="True" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Total Cost:</td>
        <td class="auto-style1">
        <asp:TextBox ID="txtActualTotalCost" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="True" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>
    </table>


            <br />
            <br />
            <br />
                <p>
                    <asp:Button ID="btnUpdateTripActuals" runat="server" CssClass="button" Text="Update Trip's Actual Cost" Width="244px" OnClick="btnUpdateTripActuals_Click"/>
                </p>

<%--div for scroll if we have a ton of trips   </div>--%>
        </div>

     </div>           
</div>        
</asp:Content>
