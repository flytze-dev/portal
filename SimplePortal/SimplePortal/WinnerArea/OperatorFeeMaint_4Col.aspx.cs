﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Objects;

namespace SimplePortal.WinnerArea
{
    public partial class OperatorFeeMaint_4Col : System.Web.UI.Page
    {
        // Instantiate sample FBO costs object
        TestWinnerCosts Winner = new TestWinnerCosts("Winner");

        protected void Page_Load(object sender, EventArgs e)
        {
            //Get current values for all Winner FeesCosts objects using functions for Account, Airport, etc. collections

            //FBO
            lblSecurityFee.Text = Winner.FeeSecurity.ToString();
            lblHandlingFee.Text = Winner.FeeHandling.ToString();
            lblParkingFee.Text = Winner.FeeParking.ToString();
            lblRampFee.Text = Winner.FeeRamp.ToString();
            lblFuelSurchargeRate.Text = Winner.FuelSurchargeRate.ToString();

            //Ops
            lblVariableSegmentFee.Text = Winner.FeeVariableSegment.ToString();
            lblFixedSegmentFee.Text = Winner.FeeFixedSegment.ToString();
            //rblSegmentFee.SelectedIndex = Winner.SegmentVariableBool;
            lblAircraftHourlyRate.Text = Winner.PlaneHourlyRate.ToString();
            lblDailyCrewCost.Text = Winner.CostDailyCrew.ToString();

            //Airport
            lblAirportConcession.Text = Winner.AirportConcessionRate.ToString();
            lblTaxiMinutes.Text = Winner.TaxiMinutes.ToString();
            lblTaxiOffset.Text = Winner.TaxiOffsetRate.ToString();
            //rbl.Text = Winner.TaxiMinutesBool.ToString();
            lblFee2ndLanding.Text = Winner.Fee2ndLanding.ToString();
            lblFixedLandingFee.Text = Winner.FeeFixedLanding.ToString();
            lblVariableLandingFee.Text = Winner.FeeVariableLanding.ToString();
            //rbl = Winner.LandingVariableBool.ToString();

            //Misc
            lblFeeCallout.Text = Winner.FeeCallout.ToString();
            //var FH = Winner.FeeHangar = 300;
            lblFeeHanger.Text = Winner.FeeHangar.ToString();
            //var FL = Winner.FeeLav = 25;
            lblFeeLav.Text = Winner.FeeLav.ToString();
            //var FGPU = Winner.FeeGPU = 10;
            lblFeeGPU.Text = Winner.FeeGPU.ToString();
            //var FO = Winner.FeeOvernight = 150;
            lblFeeOvernight.Text = Winner.FeeOvernight.ToString();
            //var F3L = Winner.Fee2ndLanding = 99;
            lblFee2ndLanding.Text = Winner.Fee2ndLanding.ToString();
            //var ENC = Winner.ExpensesNightlyCrew = 200;
            lblExpensesNightlyCrew.Text = Winner.ExpensesNightlyCrew.ToString();         
        }

        #region "FBO area"
        protected void btnEditFBO_Click(object sender, EventArgs e)
        {
            txtSecurityFee.Visible = true;
            txtHandlingFee.Visible = true;
            txtParkingFee.Visible = true;
            txtRampFee.Visible = true;
            txtFuelSurchargeRate.Visible = true;

            btnConfirmFBO.Visible = true;
            btnEditFBO.Visible = false;
            //btnEditFBO.hide = true;

            lblSecurityFee.Visible = false;
            lblHandlingFee.Visible = false;
            lblParkingFee.Visible = false;
            lblRampFee.Visible = false;
            lblFuelSurchargeRate.Visible = false;
        }

        protected void btnConfirmFBO_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtSecurityFee.Text))
            {
                Winner.FeeSecurity = Convert.ToInt32(txtSecurityFee.Text);
                lblSecurityFee.Text = Winner.FeeSecurity.ToString();
            }
            if (!String.IsNullOrEmpty(txtHandlingFee.Text))
            {
                Winner.FeeHandling = Convert.ToInt32(txtHandlingFee.Text);
                lblHandlingFee.Text = Winner.FeeHandling.ToString();
            }
            if (!String.IsNullOrEmpty(txtParkingFee.Text))
            {
                Winner.FeeParking = Convert.ToInt32(txtParkingFee.Text);
                lblParkingFee.Text = Winner.FeeParking.ToString();
            }
            if (!String.IsNullOrEmpty(txtRampFee.Text))
            {
                Winner.FeeRamp = Convert.ToInt32(txtRampFee.Text);
                txtRampFee.Text = Winner.FeeRamp.ToString();
            }
            if (!String.IsNullOrEmpty(txtFuelSurchargeRate.Text))
            {
                Winner.FuelSurchargeRate = Convert.ToInt32(txtFuelSurchargeRate.Text);
                lblFuelSurchargeRate.Text = Winner.FuelSurchargeRate.ToString();
            }

            txtSecurityFee.Visible = false;
            txtHandlingFee.Visible = false;
            txtParkingFee.Visible = false;
            txtRampFee.Visible = false;
            txtFuelSurchargeRate.Visible = false;

            btnConfirmFBO.Visible = false;
            btnEditFBO.Visible = true;
            //btnEditFBO.hide = true;

            lblSecurityFee.Visible = true;
            lblHandlingFee.Visible = true;
            lblParkingFee.Visible = true;
            lblRampFee.Visible = true;
            lblFuelSurchargeRate.Visible = true;
        }
        #endregion

        #region "Operations"
        protected void btnEditOps_Click(object sender, EventArgs e)
        {
            txtVariableSegmentFee.Visible = true;
            txtFixedSegmentFee.Visible = true;

            txtAircraftHourlyRate.Visible = true;
            txtDailyCrewCost.Visible = true;

            btnConfirmOps.Visible = true;
            btnEditOps.Visible = false;
            //btnEditFBO.hide = true;

            lblVariableSegmentFee.Visible = false;
            lblFixedSegmentFee.Visible = false;
            //lblParkingFee.Visible = false;
            lblAircraftHourlyRate.Visible = false;
            lblDailyCrewCost.Visible = false;
        }

        protected void btnConfirmOps_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtVariableSegmentFee.Text))
            {
                Winner.FeeVariableSegment = Convert.ToInt32(txtVariableSegmentFee.Text);
                lblVariableSegmentFee.Text = Winner.FeeVariableSegment.ToString();
            }
            if (!String.IsNullOrEmpty(txtFixedSegmentFee.Text))
            {
                Winner.FeeFixedSegment = Convert.ToInt32(txtFixedSegmentFee.Text);
                lblFixedSegmentFee.Text = Winner.FeeFixedSegment.ToString();
            }
            //if (!String.IsNullOrEmpty(txtFeeLav.Text))
            //{
            //    Winner.FeeParking = Convert.ToInt32(txtParkingFee.Text);
            //    lblParkingFee.Text = Winner.FeeParking.ToString();
            //}
            if (!String.IsNullOrEmpty(txtAircraftHourlyRate.Text))
            {
                Winner.PlaneHourlyRate = Convert.ToInt32(txtAircraftHourlyRate.Text);
                lblAircraftHourlyRate.Text = Winner.PlaneHourlyRate.ToString();
            }
            if (!String.IsNullOrEmpty(txtDailyCrewCost.Text))
            {
                Winner.CostDailyCrew= Convert.ToInt32(txtDailyCrewCost.Text);
                lblDailyCrewCost.Text = Winner.CostDailyCrew.ToString();
            }

            txtVariableSegmentFee.Visible = false;
            txtFixedSegmentFee.Visible = false;
            //txtParkingFee.Visible = false;
            txtAircraftHourlyRate.Visible = false;
            txtDailyCrewCost.Visible = false;

            btnConfirmOps.Visible = false;
            btnEditOps.Visible = true;
            //btnEditFBO.hide = true;

            lblVariableSegmentFee.Visible = true;
            lblFixedSegmentFee.Visible = true;
            //.Visible = true;
            lblAircraftHourlyRate.Visible = true;
            lblDailyCrewCost.Visible = true;
        }
        #endregion

        #region "Airport"
        protected void btnEditAirport_Click(object sender, EventArgs e)
        {
            txtAirportConcession.Visible = true;
            txtTaxiMinutes.Visible = true;
            txtTaxiOffset.Visible = true;
            //.Visible = true;
            txtFee2ndLanding.Visible = true;
            txtFixedLandingFee.Visible = true;
            txtVariableLandingFee.Visible = true;
            //.Visible = true;

            btnConfirmAirport.Visible = true;
            btnEditAirport.Visible = false;
            //btnEditFBO.hide = true;

            lblAirportConcession.Visible = false;
            lblTaxiMinutes.Visible = false;
            lblTaxiOffset.Visible = false;
            //.Visible = false;
            lblFee2ndLanding.Visible = false;
            lblFixedLandingFee.Visible = false;
            lblVariableLandingFee.Visible = false;
            //.Visible = false;
        }

        protected void btnConfirmAirport_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtAirportConcession.Text))
            {
                Winner.AirportConcessionRate = Convert.ToInt32(txtAirportConcession.Text);
                lblAirportConcession.Text = Winner.AirportConcessionRate.ToString();
            }
            if (!String.IsNullOrEmpty(txtTaxiMinutes.Text))
            {
                Winner.TaxiMinutes= Convert.ToInt32(txtTaxiMinutes.Text);
                lblTaxiMinutes.Text = Winner.TaxiMinutes.ToString();
            }
            if (!String.IsNullOrEmpty(txtTaxiOffset.Text))
            {
                Winner.TaxiOffsetRate= Convert.ToInt32(txtTaxiOffset.Text);
                lblTaxiOffset.Text = Winner.TaxiOffsetRate.ToString();
            }
            //if (!String.IsNullOrEmpty(txtRampFee.Text))
            //{
            //    Winner.FeeRamp = Convert.ToInt32(txtRampFee.Text);
            //    txtRampFee.Text = Winner.FeeRamp.ToString();
            //}
            if (!String.IsNullOrEmpty(txtFee2ndLanding.Text))
            {
                Winner.Fee2ndLanding= Convert.ToInt32(txtFee2ndLanding.Text);
                lblFee2ndLanding.Text = Winner.Fee2ndLanding.ToString();
            }
            if (!String.IsNullOrEmpty(txtFixedLandingFee.Text))
            {
                Winner.FeeFixedLanding = Convert.ToInt32(txtFixedLandingFee.Text);
                lblFixedLandingFee.Text = Winner.FeeFixedLanding.ToString();
            }
            if (!String.IsNullOrEmpty(txtVariableLandingFee.Text))
            {
                Winner.FeeVariableLanding = Convert.ToInt32(txtVariableLandingFee.Text);
                lblVariableLandingFee.Text = Winner.FeeVariableLanding.ToString();
            }
            //if (!String.IsNullOrEmpty(txtFee2ndLanding.Text))
            //{
            //    Winner.FuelSurchargeRate = Convert.ToInt32(txtFee2ndLanding.Text);
            //    lblFuelSurchargeRate.Text = Winner.FuelSurchargeRate.ToString();
            //}

            txtAirportConcession.Visible = false;
            txtTaxiMinutes.Visible = false;
            txtTaxiOffset.Visible = false;
            //radio button
            txtFee2ndLanding.Visible = false;
            txtFixedLandingFee.Visible = false;
            txtVariableLandingFee.Visible = false;
            //radio button

            btnConfirmAirport.Visible = false;
            btnEditAirport.Visible = true;
            //btnEditFBO.hide = true;

            lblAirportConcession.Visible = true;
            lblTaxiMinutes.Visible = true;
            lblTaxiOffset.Visible = true;
            //radio button
            lblFee2ndLanding.Visible = true;
            lblFixedLandingFee.Visible = true;
            lblVariableLandingFee.Visible = true;
            //radio button
        }
        #endregion

        #region "Misc Area"
        protected void btnEditMisc_Click(object sender, EventArgs e)
        {
            txtFeeCallout.Visible = true;
            txtFeeHanger.Visible = true;
            txtFeeLav.Visible = true;
            txtFeeGPU.Visible = true;
            txtFee2ndLanding.Visible = true;
            txtFeeOvernight.Visible = true;
            txtExpensesNightlyCrew.Visible = true;

            btnConfirmMisc.Visible = true;
            btnEditMisc.Visible = false;

            //var FC = Winner.FeeCallout;
            //var FH = Winner.FeeHangar; 
            //var FL = Winner.FeeLav;
            //var FGPU = Winner.FeeGPU;
            //var F2L = Winner.Fee2ndLanding;
            //var FO = Winner.FeeOvernight;
            //var ENC = Winner.ExpensesNightlyCrew;
            //lblFeeCallout.Text = Winner.FeeCallout.ToString();
            //lblFeeHanger.Text = Winner.FeeHangar.ToString();
            //lblFeeLav.Text = FL.ToString();
            //lblFeeGPU.Text = FGPU.ToString();
            //lblFee2ndLanding.Text = F2L.ToString();
            //lblFeeOvernight.Text = FO.ToString();
            //lblExpensesNightlyCrew.Text = ENC.ToString();

            lblFeeCallout.Visible = false;
            lblFeeHanger.Visible = false;
            lblFeeLav.Visible = false;
            lblFeeGPU.Visible = false;
            lblFee2ndLanding.Visible = false;
            lblFeeOvernight.Visible = false;
            lblExpensesNightlyCrew.Visible = false;
        }

        protected void btnConfirmMisc_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtFeeCallout.Text)) {
                Winner.FeeCallout = Convert.ToInt32(txtFeeCallout.Text);
                lblFeeCallout.Text = Winner.FeeCallout.ToString();
            }
            if (!String.IsNullOrEmpty(txtFeeHanger.Text)) {
                var FH = Winner.FeeHangar = Convert.ToInt32(txtFeeHanger.Text);
                lblFeeHanger.Text = FH.ToString();
            }
            if (!String.IsNullOrEmpty(txtFeeLav.Text))
            {
                var FL = Winner.FeeLav = Convert.ToInt32(txtFeeLav.Text);
                lblFeeLav.Text = FL.ToString();
            }
            if (!String.IsNullOrEmpty(txtFeeGPU.Text))
            {
                var FGPU = Winner.FeeGPU = Convert.ToInt32(txtFeeGPU.Text);
                lblFeeGPU.Text = FGPU.ToString();
            }
            if (!String.IsNullOrEmpty(txtFee2ndLanding.Text))
            {
                var F2L = Winner.Fee2ndLanding = Convert.ToInt32(txtFee2ndLanding.Text);
                lblFee2ndLanding.Text = F2L.ToString();
            }
            if (!String.IsNullOrEmpty(txtFeeOvernight.Text))
            {
                var FO = Winner.FeeOvernight = Convert.ToInt32(txtFeeOvernight.Text);
                lblFeeOvernight.Text = FO.ToString();
            }
            if (!String.IsNullOrEmpty(txtExpensesNightlyCrew.Text))
            {
                var ENC = Winner.ExpensesNightlyCrew = Convert.ToInt32(txtExpensesNightlyCrew.Text);
                lblExpensesNightlyCrew.Text = ENC.ToString();
            }

            lblFeeCallout.Visible = true;
            lblFeeHanger.Visible = true;
            lblFeeLav.Visible = true;
            lblFeeGPU.Visible = true;
            lblFee2ndLanding.Visible = true;
            lblFeeOvernight.Visible = true;
            lblExpensesNightlyCrew.Visible = true;

            /*
            var FC = Winner.FeeCallout = Convert.ToInt32(txtFeeCallout.Text);    
            var FH = Winner.FeeHangar = Convert.ToInt32(txtFeeHanger.Text);
            var FL = Winner.FeeLav = Convert.ToInt32(txtFeeLav.Text);
            var FGPU = Winner.FeeGPU = Convert.ToInt32(txtFeeGPU.Text);
            var F2L = Winner.Fee2ndLanding = Convert.ToInt32(txtFee2ndLanding.Text);
            var FO = Winner.FeeOvernight = Convert.ToInt32(txtFeeOvernight.Text);
            var ENC = Winner.ExpensesNightlyCrew= Convert.ToInt32(txtExpensesNightlyCrew.Text);
            
            lblFeeCallout.Text = FC.ToString();
            lblFeeHanger.Text = FH.ToString();
            lblFeeLav.Text = FL.ToString();
            lblFeeGPU.Text = FGPU.ToString();
            lblFee2ndLanding.Text = F2L.ToString();
            lblFeeOvernight.Text = FO.ToString();
            lblExpensesNightlyCrew.Text = ENC.ToString();
            */
            txtFeeCallout.Visible = false;
            txtFeeHanger.Visible = false;
            txtFeeLav.Visible = false;
            txtFeeGPU.Visible = false;
            txtFee2ndLanding.Visible = false;
            txtFeeOvernight.Visible = false;
            txtExpensesNightlyCrew.Visible = false;

            btnEditMisc.Visible = true;
            btnConfirmMisc.Visible = false;
        }
        #endregion


    }
}