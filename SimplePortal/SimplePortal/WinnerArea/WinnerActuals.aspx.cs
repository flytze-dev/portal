﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Helpers;
using SimplePortal.Objects;
using Newtonsoft.Json;

namespace SimplePortal.WinnerArea
{
    public partial class WinnerActuals : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //call function to get all Trips with actual = null or $actual values
            //AND estimated arrival < now
            //populate this list of Trips into a grid view
            gvSelectTripSubmitActuals.AllowPaging = true;

            //Hide the screen if Access Group doesn't match
            //stops people from changing url and accessing directly 
            var userAccessGroup = Session["LoggedInUserAccessGroup"];
            //if (Session["LoggedInUserAccessGroup"] == null)

            Page.Master.FindControl("MainContent").Visible = false;
            if (userAccessGroup == null)
            {
                Page.Master.FindControl("MainContent").Visible = false;
            }
            else if (userAccessGroup.ToString() == "winner")
            {
                Page.Master.FindControl("MainContent").Visible = true;
            }
            else if (userAccessGroup.ToString() == "flytzeAdmin")
            {
                Page.Master.FindControl("MainContent").Visible = true;
            }

            if (!IsPostBack)
            {
                var emptyObject = new Object();                
                var tripActualResponse = RestClientHelper.Post<RequestById>("readTrips", emptyObject, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
                //ListOfTrips TripsList = JsonConvert.DeserializeObject<ListOfTrips>(tripActualResponse.FunctionResponse.ToString());
                List<Trip> TripsList = JsonConvert.DeserializeObject<List<Trip>>(tripActualResponse.FunctionResponse.ToString());
                //ListOfTrips TripsList = JsonConvert.DeserializeObject<ListOfTrips>(tripActualResponse.FunctionResponse.ToString());

                Session["TripsList"] = TripsList;
                gvSelectTripSubmitActuals.DataSource = TripsList;
                gvSelectTripSubmitActuals.DataBind();
                //GridView1.DataKeyNames = "id";

                if (TripsList.Count < 1)
                {
                    lblNoTrips.Visible = true;
                    lblNoTrips.Text = "There are no past trips that need actual values updated.";
                }
            }

            //DOn't know why this doesn't work
            //this.Page.Master.FindControl("LeftHalf").Visible = false;
            //this.Page.FindControl("rightHalf").Visible = false;

            btnUpdateTripActuals.Visible = false;
            TableActualTripInfo.Visible = false;

            //TableActualTripInfo.Visible = false;

            //lblTest0.Text = TripsList[0].arrival.airportCode.ToString();
            //lblTest1.Text = TripsList[1].arrival.stateName.ToString();
            //lblTest2.Text = TripsList[2].arrival.stateName.ToString();
        }

        protected void gvSelectTripSubmitActuals_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int index = Convert.ToInt16(GridView1.SelectedDataKey.Value);
            //lblTest0.Text = "Going to edit row: " + index.ToString();

            TableActualTripInfo.Visible = true;
            btnUpdateTripActuals.Visible = true;
            lblUpdateTripActual.Visible = true;

            gvSelectTripSubmitActuals.Visible = false;
            int index = gvSelectTripSubmitActuals.SelectedIndex;

            lblUpdateTripActual.Visible = true;
            var selectedTripId = gvSelectTripSubmitActuals.DataKeys[index].Value.ToString();
            lblUpdateTripActual.Text = "Editing Trip ID: " + selectedTripId;

            Session["UpdatedTripId"] = selectedTripId;

            ////ListOfTrips getTripsList = (ListOfTrips)Session["TripsList"];
            //List<TripActual> getTripsList = (List<TripActual>)Session["TripsList"];
            //var tripsArray = getTripsList.ToArray();
            //var tripSelected = tripsArray.Where(TripActual)
            ////getTripsList

            //DOn't know why this doesn't work
            //this.Page.FindControl("LeftHalf").Visible = true;
            //this.Page.FindControl("rightHalf").Visible = true;

            ////THis is to view trip in expanded version we will release soon
            //var tripToSelect = new RequestById();
            //tripToSelect.id = selectedTripId;
            //Trip SelectedTrip = JsonConvert.DeserializeObject<Trip>(getTripInfo.FunctionResponse.ToString());



            //TableActualTripInfo.Visible = true;
            ////txtActualMisc.Visible = true;
            ////txtActualDailyCrew.Visible = true;
            ////txtActualFuelSurcharge.Visible = true;
            ////txtActualFlightCharges.Visible = true;
            //txtActualSubtotal.Visible = true;
            //txtActualSegmentFee.Visible = true;
            //txtActuaAirportConcession.Visible = true;
            //txtActualTotalTaxes.Visible = true;
            //txtActualTotalCost.Visible = true;
            ////txtActualArrivalTime.Visible = true;
            ////txtActualFlightDistance.Visible = true;
            ////txtActualFlightDuration.Visible = true;
            ////txtActualAirportConcession.Visible = true;
            ////txtBaseRate.Visible = true;
            ////txtSegmentFee.Visible = true;
            ////txtActualTotalTaxes.Visible = true;
            ////txtActualTotalCost.Visible = true;

        }

        protected void btnUpdateTripActuals_Click(object sender, EventArgs e)
        {
            //ActualTotalCost actualTotalCost = new ActualTotalCost();

            #region "Old with times and 5 value update included, may return"

            UpdateTrip updateTripActual = new UpdateTrip();
            updateTripActual.id = Session["UpdatedTripId"].ToString();

            //I wonder why this doesn't work, it seems so simple and correct.
            //updateTripActual.departure.actual = txtActualDepartureTime.Text.ToString();
            //updateTripActual.arrival.actual = txtActualArrivalTime.Text.ToString();
            //updateTripActual.flightDistance.actual = Convert.ToDouble(txtActualFlightDistance.Text);
            //updateTripActual.flightTime.actual = Convert.ToDouble(txtActualFlightDuration.Text);
            //updateTripActual.actualCost.baseRate = Convert.ToDouble(txtBaseRate.Text);
            //updateTripActual.actualCost.taxes.total = Convert.ToDouble(txtActualTotalTaxes.Text);
            //updateTripActual.actualCost.total = Convert.ToDouble(txtActualTotalCost.Text);

            var empty = 0;
            var ActualCostForTrip = new ActualTotalCost();
            ActualCostForTrip.baseRate = empty;
            if (!String.IsNullOrEmpty(txtBaseRate.Text))
            {
                ActualCostForTrip.baseRate = Convert.ToDouble(txtBaseRate.Text);
            }
            ActualCostForTrip.total = empty;
            if (!String.IsNullOrEmpty(txtActualTotalCost.Text))
            {
                ActualCostForTrip.total = Convert.ToDouble(txtActualTotalCost.Text);
            }

            var Taxes = new Taxes();
            Taxes.total = empty;
            if (!String.IsNullOrEmpty(txtActualTotalTaxes.Text))
            {
                Taxes.total = Convert.ToDouble(txtActualTotalTaxes.Text);
            }

            var ActualTripDeparture = new ActualString();
            ActualTripDeparture.actual = empty.ToString();
            if (!String.IsNullOrEmpty(txtActualDepartureTime.Text))
            {
                ActualTripDeparture.actual = txtActualDepartureTime.Text.ToString();
            }

            var ActualTripArrival = new ActualString();
            ActualTripArrival.actual = empty.ToString();
            if (!String.IsNullOrEmpty(txtActualArrivalTime.Text))
            {
                ActualTripArrival.actual = txtActualArrivalTime.Text.ToString();
            }

            var ActualTripFlightDistance = new ActualDouble();
            ActualTripFlightDistance.actual = empty;
            if (!String.IsNullOrEmpty(txtActualFlightDistance.Text))
            {
                ActualTripFlightDistance.actual = Convert.ToDouble(txtActualFlightDistance.Text);
            }

            var ActualTripFlightTime = new ActualDouble();
            ActualTripFlightTime.actual = empty;
            if (!String.IsNullOrEmpty(txtActualFlightDuration.Text))
            {
                ActualTripFlightTime.actual = Convert.ToDouble(txtActualFlightDuration.Text);
            }

            ActualCostForTrip.taxes = Taxes;
            updateTripActual.actualCost = ActualCostForTrip;
            updateTripActual.arrival = ActualTripArrival;
            updateTripActual.departure = ActualTripDeparture;
            updateTripActual.flightDistance = ActualTripFlightDistance;
            updateTripActual.flightTime = ActualTripFlightTime;            

            Session["passTripUpdate"] = updateTripActual;
            #endregion



            //actualTotalCost.subTotal = Convert.ToDouble(txtActualsubTotal.Text);
            //actualTotalCost.subTotal = txtActualsubTotal.Text.ToString();


            //var updateTripActual = new UpdateTripActuals();
            updateTripActual.id = Session["UpdatedTripId"].ToString();
            //updateTripActual.actualCost = actualTotalCost;            
            Session["passTripUpdate"] = updateTripActual;

            var updateTripResponse = RestClientHelper.Post<UpdateTrip>("updateTrip", updateTripActual, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());

            

            var request = new RequestById();
            request.id = Session["UpdatedTripId"].ToString();
            var caller = Session["LoggedInUser"];
            var tripId = Session["UpdatedTripId"].ToString();
            var balanceFlytzeChargesResponse = RestClientHelper.Post<RequestById>("balanceFlytzeCharges", request, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
            
            var fboToDisplay = new RequestById();
            //fboToDisplay.id = "56f42b695e38a90f42f2e7c2";
            fboToDisplay.id = "55a74297e4b07e03cc6ac65e";


            //var emptyObject = new Object();
            //var result = balanceFlytzeChargesResponse.FunctionResponse.ToString();

            //Session["balanceFlytzeResult"] = result;
            Session["balanceFlytzeResult"] = balanceFlytzeChargesResponse.FunctionResponse.ToString();

            Response.Redirect("~/WinnerArea/UpdateTripConfirmation.aspx");

        }

        protected void gvSelectTripSubmitActuals_PageIndexChanging(object sender, EventArgs e)
        {
            //make paging happen
            //gvSelectTripSubmitActuals.PageIndex = e.NewPageIndex;
            //BindData();

        }
    }
}