﻿<%@ Page Title="Winner" Language="C#" MasterPageFile="~/MasterPages/Portal.Master" AutoEventWireup="true" CodeBehind="WinnerFees.aspx.cs" Inherits="SimplePortal.WinnerArea.WinnerFees" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle" runat="server">
        Maintain Default Fee Settings 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="outter" id="all">
<div class="col-md-3 BorderBuffer">
    <div class="ContentContainer" id="left">
        <h3>Operator Charges</h3>
        <div class = "SmallFont">
        <table id="TableEditMisc2">    
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td >Amount</td>
    </tr> 
    <tr>
        <td>Security Fee:</td>
        <td class="auto-style1">
            <asp:Label ID="lblSecurityFee" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtSecurityFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Handling Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblHandlingFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtHandlingFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Parking Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblParkingFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtParkingFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Ramp Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblRampFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtRampFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Fuel Surcharge Rate:</td>
        <td class="auto-style1"><asp:Label ID="lblFuelSurchargeRate" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFuelSurchargeRate" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>

    <tr>
        <td>Segment Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblSegmentFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtSegmentFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" BackColor="#E9E9E9" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>

    </table>
</div >
    <div >
    <p>       
        
<%--        <input type="button" id="Button1" runat="server" onclick="btnEditFBO_Click" text="Edit FBO" cssclass="button" />
        <input type="button" id="Button2" class="button" runat="server" onclick="btnConfirmFBO_Click" text="Confirm FBO" Visible="False" width="127px" cssclass="button" />
    --%>
    </p>
        <p>
      <span>  
        <asp:Button ID="btnEditFBO" runat="server" OnClick="btnEditFBO_Click" Text="Edit FBO" CssClass="button" />
          </span>
        <asp:Button ID="btnConfirmFBO" class="button" runat="server" OnClick="btnConfirmFBO_Click" Text="Confirm FBO" Visible="False" Width="126px" CssClass="button" />
    </p>
    </div>
</div>
</div>

<div class ="col-md-3">
    <div class="ContentContainer">
<h3>Flight Operations</h3>
        <div class = "SmallFont">
    <table ID="TableEditMisc1">
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td >Amount</td>
    </tr>      
    <tr>
        <td colspan="2">Fixed or Variable Segment Fee?<br />
            <asp:RadioButtonList CssClass ="inline-rb" ID="rblSegmentIsFixed" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" BorderStyle="None">
            <asp:ListItem >Variable</asp:ListItem>
            <asp:ListItem >Fixed</asp:ListItem>
            </asp:RadioButtonList>

        </td>
    </tr>    

    <tr>
        <td>Daily Crew Cost:</td>
        <td class="auto-style1"><asp:Label ID="lblDailyCrewCost" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtDailyCrewCost" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
        <tr>
        <td>Airport Concession Rate:</td>
        <td class="auto-style1"><asp:Label ID="lblAirportConcession" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtAirportConcession" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
        </tr>
    </table>
            <p>
&nbsp;</p>
            <p>
                <asp:Button ID="btnEditOps" runat="server" OnClick="btnEditOps_Click" Text="Edit Ops" CssClass="button" />
                <asp:Button ID="btnConfirmOps" runat="server" OnClick="btnConfirmOps_Click" Text="Confirm Ops" Visible="False" Width="127px" CssClass="button" />
            </p>
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
</div>
</div>
</div>

<%--
<div class ="col-md-3 ">
    <div class="ContentContainer">
<h3>Airport</h3>
    <div class = "SmallFont">
    <table ID="TableEditMisc0">
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td class="auto-style1">Amount</td>
    </tr>      
    <tr>
        <td>Taxi Time (Minutes):</td>
        <td class="auto-style1">
            <asp:Label ID="lblTaxiMinutes" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtTaxiMinutes" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Taxi Time (Offset Rate %):</td>
        <td class="auto-style1"><asp:Label ID="lblTaxiOffset" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtTaxiOffset" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>

    </tr>    
    <tr>
        <td colspan="2">TaxiTaxi Time: Minutes or Offset % 
            <asp:RadioButtonList CssClass ="inline-rb" ID="rblTaxiUseMinutes" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" BorderStyle="None" Width="289px">
            <asp:ListItem>Offset %</asp:ListItem>
            <asp:ListItem >Average Minutes</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>    
    <tr>  --%>
        <%--<td>2nd Landing Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFee2ndLanding" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFee2ndLanding" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>--%>
<%--    </tr>
    <tr>
        <td style="height: 27px">Landing Fee:</td>
        <td class="auto-style1" style="height: 27px"><asp:Label ID="lblLandingFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtLandingFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td colspan="2">LandLanding Fee: Fixed or Variable?<asp:RadioButtonList CssClass ="inline-rb" ID="rblLandingIsFixed" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" BorderStyle="None" Height="16px">
            <asp:ListItem>Variable</asp:ListItem>
            <asp:ListItem >Fixed</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>    
    </table>
</div>
    <p>
        &nbsp;</p>
        <p>
        <asp:Button ID="btnEditAirport" runat="server" OnClick="btnEditAirport_Click" Text="Edit Airport" CssClass="button" />
        <asp:Button ID="btnConfirmAirport" runat="server" OnClick="btnConfirmAirport_Click" Text="Confirm Airport" Visible="False" Width="155px" CssClass="button" />
    </p>
</div>
</div>
--%>

<div class = "col-md-3 ">
    <div class="ContentContainer">
<h3>Aircraft </h3>
        
        <table id="TableEditMisc3">
    <tr class="TableTitles">
        <%--<td>Select Aircraft</td>
        <td class="auto-style1">
            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="getAircraft" DataTextField="Success" DataValueField="Success">
            </asp:DropDownList>
        </td>--%>
    </tr>      
        </table>
    <div class = "SmallFont" title =" test definition area">
    <%--<table ID="TableEditMisc">
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td class="auto-style1">Amount</td>
    </tr>      
    <tr>
        <td title="Explain Callout Fee.">FeeCallout:</td>
        <td class="auto-style1">
            <asp:Label ID="lblFeeCallout" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtFeeCallout" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Hangar Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFeeHanger" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFeeHanger" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Lav Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFeeLav" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFeeLav" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Ground Power Unit Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFeeGPU" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFeeGPU" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Overnight Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFeeOvernight" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFeeOvernight" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Nightly Crew Expense:</td>
        <td class="auto-style1"><asp:Label ID="lblExpensesNightlyCrew" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtExpensesNightlyCrew" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    </table>--%>


<table ID="TableEditMisc">
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td class="auto-style1">Amount</td>
    </tr>      
    <tr>
        <td title="Explain Callout Fee.">Hourly Rate:</td>
        <td class="auto-style1">
            <asp:Label ID="lblAircraftHrRate" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtAircraftHrRate" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Average Speed:</td>
        <td class="auto-style1"><asp:Label ID="lblAvgSpeed" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtAvgSpeed" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Min Runway Length Required:</td>
        <td class="auto-style1"><asp:Label ID="lblReqRunway" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtReqRunway" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Engine Type:</td>
        <td class="auto-style1"><asp:Label ID="lblEngine" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtEngine" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Number of Seats:</td>
        <td class="auto-style1"><asp:Label ID="lblSeatCount" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtSeatCount" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Tail Number:</td>
        <td class="auto-style1"><asp:Label ID="lblTailNumber" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtTailNumber" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
</table>
</div>
    <p>
        &nbsp;</p>
        <p>
        <asp:Button ID="btnEditAircraft" runat="server" OnClick="btnEditAircraft_Click" Text="Edit Aircraft" CssClass="button" />
        <asp:Button ID="btnConfirmAircraft" runat="server" OnClick="btnConfirmAircraft_Click" Text="Confirm Aircraft" Visible="False" Width="155px" CssClass="button" />
    </p>
</div>
</div>



<div class ="col-md-3 ">
    <div class="ContentContainer">
<h3>Taxes</h3>
    <div class = "SmallFont">
    <table id="TableEditMisc0">
    <tr class="TableTitles">
        <td>Tax</td>
        <td class="auto-style1">Charge</td>
    </tr>      
    <tr>
        <td>FET:</td>
        <td class="auto-style1">
            <asp:Label ID="lblFETTax" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtFETTax" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>WRPA:</td>
        <td class="auto-style1"><asp:Label ID="lblWRPA" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtWRPA" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>

    </tr>    
    <tr>
        <td colspan="2">State Tax:</td>
    </tr>    
    <tr>
        <%--<td>2nd Landing Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFee2ndLanding" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFee2ndLanding" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>--%>
    </tr>
    <tr>
        <td style="height: 27px">Ohio:</td>
        <td class="auto-style1" style="height: 27px"><asp:Label ID="lblOhioTax" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtOhioTax" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" CssClass="textbox" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>    
    </table>
</div>
    <p>
        &nbsp;</p>
        <p>
        <asp:Button ID="btnEditTaxes" runat="server" OnClick="btnEditTaxes_Click" Text="Edit Taxes" CssClass="button" />
        <asp:Button ID="btnConfirmTaxes" runat="server" OnClick="btnConfirmTaxes_Click" Text="Confirm Taxes" Visible="False" Width="155px" CssClass="button" />
    </p>
</div>
</div>

</div>
</asp:Content>
