﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Objects;
using SimplePortal.Helpers;
using Newtonsoft.Json;

namespace SimplePortal.WinnerArea
{
    public partial class WinnerFees : System.Web.UI.Page
    {

        //Winner Account that feeds quote= _id:ObjectId("55a74297e4b07e03cc6ac65e")
        //Winner Account that can login/edit = 
        WinnerCosts WinnerCostObject = new WinnerCosts();
        //Aircraft WinnerAircraftObject = new Aircraft();
        Fees WinnerFeesObject = new Fees();
        landingFee landingFeeObject = new landingFee();
        segmentFee segmentFeeObject = new segmentFee();
        taxiTime taxiTimeObject = new taxiTime();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoggedInUserAccessGroup"] != null && Session["LoggedInUser"] != null)
            {
                var loggedInUserToken = Session["LoggedInUserToken"];
                var loggedInUser = Session["LoggedInUser"];
                var loggedInUserAccessGroup = Session["LoggedInUserAccessGroup"];
                //var profileUsername = Session["LoggedInUser"].ToString();
                ////Hide the screen if Access Group doesn't match
                ////stops people from changing url and accessing directly 

                //var userAccessGroup = Session["LoggedInUserAccessGroup"];

                ////if (Session["LoggedInUserAccessGroup"] == null)

                //Page.Master.FindControl("MainContent").Visible = false;
                //if (userAccessGroup == null)
                //{
                //    Page.Master.FindControl("MainContent").Visible = false;
                //}
                //else if (userAccessGroup.ToString() == "winner")
                //{
                //    Page.Master.FindControl("MainContent").Visible = true;
                //}
                //else if (userAccessGroup.ToString() == "flytzeAdmin")
                //{
                //    Page.Master.FindControl("MainContent").Visible = true;
                //}


                //var loggedInUserToken = Session["LoggedInUserToken"];
                //var loggedInUser = Session["LoggedInUser"];

                #region "Initiate FBO"

                if (!IsPostBack)
                {
                    var fboToDisplay = new RequestById();
                    //fboToDisplay.id = "56f42b695e38a90f42f2e7c2";
                    fboToDisplay.id = "55a74297e4b07e03cc6ac65e";

                    var fboResponse = RestClientHelper.Post<WinnerCosts>("readFbo", fboToDisplay, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
                    //var fboResponse = RestClientHelper.Post<WinnerCosts>("readFbo", fboToDisplay);
                    WinnerCostObject = JsonConvert.DeserializeObject<WinnerCosts>(fboResponse.FunctionResponse.ToString());

                    //Session["FBOObject"] = WinnerCostObject;
                    //WinnerFeesObject = JsonConvert.DeserializeObject<winnerFees>(WinnerCostObject.ToString());

                    lblSecurityFee.Text = WinnerCostObject.fees.security.ToString();
                    lblHandlingFee.Text = WinnerCostObject.fees.handling.ToString();
                    lblParkingFee.Text = WinnerCostObject.fees.parking.ToString();
                    lblRampFee.Text = WinnerCostObject.fees.ramp.ToString();
                    lblFuelSurchargeRate.Text = WinnerCostObject.fees.fuelSurchargeRate.ToString();
                    lblSegmentFee.Text = WinnerCostObject.fees.segment.fee.ToString();

                    txtSecurityFee.Visible = false;
                    txtSecurityFee.Text = WinnerCostObject.fees.security.ToString();
                    txtHandlingFee.Visible = false;
                    txtHandlingFee.Text = WinnerCostObject.fees.handling.ToString();
                    txtParkingFee.Visible = false;
                    txtParkingFee.Text = WinnerCostObject.fees.parking.ToString();
                    txtRampFee.Visible = false;
                    txtRampFee.Text = WinnerCostObject.fees.ramp.ToString();
                    txtFuelSurchargeRate.Visible = false;
                    txtFuelSurchargeRate.Text = WinnerCostObject.fees.fuelSurchargeRate.ToString();
                    txtSegmentFee.Visible = false;
                    txtSegmentFee.Text = WinnerCostObject.fees.segment.fee.ToString();

                    //}
                    #endregion

                    #region "Initiate Ops"
                    //rblSegmentFee.SelectedIndex = WinnerCostObject.fees.segment.isFixed;
                    //var whatisit = Convert.ToInt16(WinnerCostObject.fees.segment.isFixed);
                    //Label2.Text = whatisit.ToString();
                    //rblSegmentFee.SelectedIndex = whatisit;
                    rblSegmentIsFixed.SelectedIndex = Convert.ToInt32(WinnerCostObject.fees.segment.isFixed);
                    rblSegmentIsFixed.Enabled = false;

                    lblDailyCrewCost.Text = WinnerCostObject.fees.dailyCrewCost.ToString();
                    lblAirportConcession.Text = WinnerCostObject.fees.airportConcession.ToString();

                    //rbl needed?
                    txtDailyCrewCost.Text = WinnerCostObject.fees.dailyCrewCost.ToString();
                    txtAirportConcession.Text = WinnerCostObject.fees.airportConcession.ToString();
                    #endregion

                    #region "Initiate Airport"
                    //////Initiate Airport - from Account
                    //lblFETTax.Text = WinnerCostObject.fees.taxiTime.minutes.ToString();
                    //lblWRPA.Text = WinnerCostObject.fees.taxiTime.offset.ToString();
                    //rblTaxiUseMinutes.SelectedIndex = Convert.ToInt32(WinnerCostObject.fees.taxiTime.useMinutes);
                    //rblTaxiUseMinutes.Enabled = false;

                    //lblOhioTax.Text = WinnerCostObject.fees.landing.fee.ToString();
                    //rblLandingIsFixed.SelectedIndex = Convert.ToInt32(WinnerCostObject.fees.landing.isFixed);
                    //rblLandingIsFixed.Enabled = false;

                    //txtFETTax.Text = WinnerCostObject.fees.taxiTime.minutes.ToString();
                    //txtWRPA.Text = WinnerCostObject.fees.taxiTime.offset.ToString();
                    ////string offsetPercent = WinnerCostObject.fees.taxi.offset.ToString();                
                    ////txtTaxiOffset.Text = String.Format("{0:P2}"*100"%", offsetPercent);                
                    ////rbl?
                    //txtOhioTax.Text = WinnerCostObject.fees.landing.fee.ToString();
                    ////rbl?
                    #endregion

                    #region "Initiate Aircraft"
                    //Initiate Aircraft from Aircraft
                    var aircraftToDisplay = new RequestById();
                    aircraftToDisplay.id = "55a74198e4b07e03cc6ac65a";
                    //aircraftToDisplay.amount = 0;
                    //aircraftToDisplay.email = "q";
                    var aircraftResponse = RestClientHelper.Post<Aircraft>("readPlane", aircraftToDisplay, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
                    Aircraft aircraftDisplayed = JsonConvert.DeserializeObject<Aircraft>(aircraftResponse.FunctionResponse.ToString());

                    ////Session["aircraft"] = aircraftDisplayed;

                    lblAircraftHrRate.Text = aircraftDisplayed.fees.hourlyRate.ToString();
                    lblAvgSpeed.Text = aircraftDisplayed.speed.ToString();
                    lblReqRunway.Text = aircraftDisplayed.runwayLength.ToString();
                    lblEngine.Text = aircraftDisplayed.engine.ToString();
                    lblSeatCount.Text = aircraftDisplayed.seatCount.ToString();
                    lblTailNumber.Text = aircraftDisplayed.tailNumber.ToString();

                    txtAircraftHrRate.Text = aircraftDisplayed.fees.hourlyRate.ToString();
                    txtAvgSpeed.Text = aircraftDisplayed.speed.ToString();
                    txtReqRunway.Text = aircraftDisplayed.runwayLength.ToString();
                    txtEngine.Text = aircraftDisplayed.engine.ToString();
                    txtSeatCount.Text = aircraftDisplayed.seatCount.ToString();
                    txtTailNumber.Text = aircraftDisplayed.tailNumber.ToString();

                    #endregion

                    #region "Initiate Tax"
                    var viewtax = new RequestById();
                    viewtax.id = "56d139b7c1b0bac5e99f4831";
                    var taxResponse = RestClientHelper.Post<taxWinner>("readTaxWinner", viewtax, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
                    taxWinner taxDisplayed = JsonConvert.DeserializeObject<taxWinner>(taxResponse.FunctionResponse.ToString());

                    Session["taxes"] = taxDisplayed;

                    lblFETTax.Text = taxDisplayed.fetRate.ToString();
                    lblWRPA.Text = taxDisplayed.wrpaRate.ToString();
                    lblOhioTax.Text = taxDisplayed.state.OH.ToString();
                    txtFETTax.Text = taxDisplayed.fetRate.ToString();
                    txtWRPA.Text = taxDisplayed.wrpaRate.ToString();
                    txtOhioTax.Text = taxDisplayed.state.OH.ToString();

                    txtFETTax.Visible = false;
                    txtWRPA.Visible = false;
                    txtOhioTax.Visible = false;
                    #endregion
                }
            }
        }

        #region "FBO area"
        protected void btnEditFBO_Click(object sender, EventArgs e)
{
    //var fboToDisplay = new RequestById();
    //fboToDisplay.id = "56f42b695e38a90f42f2e7c2";
    //var fboResponse = RestClientHelper.Post<WinnerCosts>("readFbo", fboToDisplay);
    //WinnerCosts WinnerObject = JsonConvert.DeserializeObject<WinnerCosts>(fboResponse.FunctionResponse.ToString());

    txtSecurityFee.Visible = true;
    //txtSecurityFee.Text = WinnerObject.fees.security.ToString();
    txtHandlingFee.Visible = true;
    //txtHandlingFee.Text = WinnerObject.fees.handling.ToString();
    txtParkingFee.Visible = true;
    //txtParkingFee.Text = WinnerObject.fees.parking.ToString();
    txtRampFee.Visible = true;
    //txtRampFee.Text = WinnerObject.fees.ramp.ToString();
    txtFuelSurchargeRate.Visible = true;
    //txtFuelSurchargeRate.Text = WinnerObject.fees.fuelSurchargeRate.ToString();
    txtSegmentFee.Visible = true;

    btnConfirmFBO.Visible = true;
    btnEditFBO.Visible = false;
    //btnEditFBO.hide = true;
    btnEditAircraft.Visible = false;
    btnEditTaxes.Visible = false;
    btnEditOps.Visible = false;

    lblSecurityFee.Visible = false;
    lblHandlingFee.Visible = false;
    lblParkingFee.Visible = false;
    lblRampFee.Visible = false;
    lblFuelSurchargeRate.Visible = false;
    lblSegmentFee.Visible = false;
}

protected void btnConfirmFBO_Click(object sender, EventArgs e)
{
        var fboToDisplay = new RequestById();
        fboToDisplay.id = "55a74297e4b07e03cc6ac65e";        
        var fboResponse = RestClientHelper.Post<WinnerCosts>("readFbo", fboToDisplay, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
        WinnerCosts WinnerCostObject = JsonConvert.DeserializeObject<WinnerCosts>(fboResponse.FunctionResponse.ToString());


        //WinnerCosts WinnerCostObject = (WinnerCosts)Session["FBOObject"];

            if (!String.IsNullOrEmpty(txtSecurityFee.Text))
    {
        WinnerCostObject.fees.security = Convert.ToInt64(txtSecurityFee.Text);
        lblSecurityFee.Text = WinnerCostObject.fees.security.ToString();
    }
    if (!String.IsNullOrEmpty(txtHandlingFee.Text))
    {
        WinnerCostObject.fees.handling = Convert.ToInt64(txtHandlingFee.Text);
        lblHandlingFee.Text = WinnerCostObject.fees.handling.ToString();
    }
    if (!String.IsNullOrEmpty(txtParkingFee.Text))
    {
        WinnerCostObject.fees.parking = Convert.ToInt64(txtParkingFee.Text);
        lblParkingFee.Text = WinnerCostObject.fees.parking.ToString();
    }
    if (!String.IsNullOrEmpty(txtRampFee.Text))
    {
        WinnerCostObject.fees.ramp = Convert.ToInt64(txtRampFee.Text);
        lblRampFee.Text = WinnerCostObject.fees.ramp.ToString();
    }
    if (!String.IsNullOrEmpty(txtFuelSurchargeRate.Text))
    {
        WinnerCostObject.fees.fuelSurchargeRate = Convert.ToInt64(txtFuelSurchargeRate.Text);
        lblFuelSurchargeRate.Text = WinnerCostObject.fees.fuelSurchargeRate.ToString();
    }
    if (!String.IsNullOrEmpty(txtSegmentFee.Text))
    {
        WinnerCostObject.fees.segment.fee = Convert.ToInt64(txtSegmentFee.Text);
        lblSegmentFee.Text = WinnerCostObject.fees.segment.fee.ToString();
    }

    var fboUpdateToSend = new sendUpdate();
    fboUpdateToSend.id = "55a74297e4b07e03cc6ac65e";
    fboUpdateToSend.updates = WinnerCostObject;
    var fboUpdateResponse = RestClientHelper.Post<WinnerCosts>("updateFbo", fboUpdateToSend, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
    var isSucess = fboUpdateResponse.Success.ToString();
          
    txtSegmentFee.Visible = false;
    txtSecurityFee.Visible = false;
    txtHandlingFee.Visible = false;
    txtParkingFee.Visible = false;
    txtRampFee.Visible = false;
    txtFuelSurchargeRate.Visible = false;

    btnConfirmFBO.Visible = false;
    btnEditFBO.Visible = true;
    //btnEditFBO.hide = true;
    btnEditAircraft.Visible = true;
    btnEditTaxes.Visible = true;
    btnEditOps.Visible = true;

    lblSecurityFee.Visible = true;
    lblHandlingFee.Visible = true;
    lblParkingFee.Visible = true;
    lblRampFee.Visible = true;
    lblFuelSurchargeRate.Visible = true;
    lblSegmentFee.Visible = true;
    }
#endregion

#region "Operations Area"
protected void btnEditOps_Click(object sender, EventArgs e)
{

    rblSegmentIsFixed.Enabled = true;
    txtDailyCrewCost.Visible = true;
    txtAirportConcession.Visible = true;

    btnConfirmOps.Visible = true;
    btnEditOps.Visible = false;
    btnEditAircraft.Visible = false;
    btnEditTaxes.Visible = false;
    btnEditFBO.Visible = false;

    lblDailyCrewCost.Visible = false;
    lblAirportConcession.Visible = false;
}

protected void btnConfirmOps_Click(object sender, EventArgs e)
{
    var fboToDisplay = new RequestById();
    fboToDisplay.id = "55a74297e4b07e03cc6ac65e";
    var fboResponse = RestClientHelper.Post<WinnerCosts>("readFbo", fboToDisplay, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
    WinnerCosts WinnerCostObject = JsonConvert.DeserializeObject<WinnerCosts>(fboResponse.FunctionResponse.ToString());


    //WinnerCosts WinnerCostObject = (WinnerCosts)Session["FBOObject"];

    //var sendFboUpdate = new UpdateFbo("56f42b695e38a90f42f2e7c2");
    //sendFboUpdate.id = "56f42b695e38a90f42f2e7c2";



        WinnerCostObject.fees.segment.isFixed = Convert.ToBoolean(rblSegmentIsFixed.SelectedIndex);

    if (!String.IsNullOrEmpty(txtDailyCrewCost.Text))
    {
        WinnerCostObject.fees.dailyCrewCost = Convert.ToInt64(txtDailyCrewCost.Text);
        lblDailyCrewCost.Text = WinnerCostObject.fees.dailyCrewCost.ToString();
    }
    if (!String.IsNullOrEmpty(txtAirportConcession.Text))
    {
        WinnerCostObject.fees.airportConcession = Convert.ToDouble(txtAirportConcession.Text);
        lblAirportConcession.Text = WinnerCostObject.fees.airportConcession.ToString();
    }

    var opsUpdateToSend = new sendUpdate();
    opsUpdateToSend.id = "55a74297e4b07e03cc6ac65e";
    opsUpdateToSend.updates = WinnerCostObject;
    var fboUpdateResponse = RestClientHelper.Post<sendUpdate>("updateFbo", opsUpdateToSend, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
    var isSucess = fboUpdateResponse.Success.ToString();

    rblSegmentIsFixed.Enabled = false;
    txtDailyCrewCost.Visible = false;
    txtAirportConcession.Visible = false;

    btnConfirmOps.Visible = false;
    btnEditOps.Visible = true;
    btnEditAircraft.Visible = true;
    btnEditTaxes.Visible = true;
    btnEditFBO.Visible = true;


    lblAirportConcession.Visible = true;
    lblDailyCrewCost.Visible = true;
}
        #endregion

        //#region "Airport Area"
        //protected void btnEditAirport_Click(object sender, EventArgs e)
        //{
        //    txtFETTax.Visible = true;
        //    rblTaxiUseMinutes.Enabled = true;
        //    txtWRPA.Visible = true;
        //    txtOhioTax.Visible = true;
        //    rblLandingIsFixed.Enabled = true;

        //    btnConfirmTaxes.Visible = true;
        //    btnEditTaxes.Visible = false;
        //    btnEditAircraft.Visible = false;
        //    btnEditFBO.Visible = false;
        //    btnEditOps.Visible = false;

        //    lblFETTax.Visible = false;
        //    lblWRPA.Visible = false;
        //    lblOhioTax.Visible = false;
        //}

        //protected void btnConfirmAirport_Click(object sender, EventArgs e)
        //{
        //            var fboToDisplay = new RequestById();
        //            fboToDisplay.id = "56f42b695e38a90f42f2e7c2";
        //            var WinnerCostObject = JsonConvert.DeserializeObject<WinnerCosts>(fboResponse.FunctionResponse.ToString());

        //            //WinnerCosts WinnerCostObject = (WinnerCosts)Session["FBOObject"];

        //            if (!String.IsNullOrEmpty(txtFETTax.Text))
        //    {
        //        WinnerCostObject.fees.taxiTime.minutes = Convert.ToInt64(txtFETTax.Text);
        //        lblFETTax.Text = WinnerCostObject.fees.taxiTime.minutes.ToString();
        //    }
        //    if (!String.IsNullOrEmpty(txtWRPA.Text))
        //    {
        //        WinnerCostObject.fees.taxiTime.offset = Convert.ToDouble(txtWRPA.Text);
        //        lblWRPA.Text = WinnerCostObject.fees.taxiTime.offset.ToString();
        //    }

        //        WinnerCostObject.fees.taxiTime.useMinutes = Convert.ToBoolean(rblTaxiUseMinutes.SelectedIndex);

        //    if (!String.IsNullOrEmpty(txtOhioTax.Text))
        //    {
        //        WinnerCostObject.fees.landing.fee = Convert.ToInt64(txtOhioTax.Text);
        //        lblOhioTax.Text = WinnerCostObject.fees.landing.fee.ToString();
        //    }

        //        WinnerCostObject.fees.landing.isFixed = Convert.ToBoolean(rblLandingIsFixed.SelectedIndex);

        //    var airportUpdateToSend = new sendUpdate();
        //    airportUpdateToSend.id = "56f42b695e38a90f42f2e7c2";
        //    airportUpdateToSend.updates = WinnerCostObject;
        //    var isSucess = fboUpdateResponse.Success.ToString();

        //    txtFETTax.Visible = false;
        //    txtWRPA.Visible = false;
        //    rblTaxiUseMinutes.Enabled = false;
        //    txtOhioTax.Visible = false;
        //    rblLandingIsFixed.Enabled = false;

        //    btnConfirmTaxes.Visible = false;
        //    btnEditTaxes.Visible = true;
        //    btnEditAircraft.Visible = true;
        //    btnEditFBO.Visible = true;
        //    btnEditOps.Visible = true;

        //    lblFETTax.Visible = true;
        //    lblWRPA.Visible = true;
        //    //radio button
        //    lblOhioTax.Visible = true;
        //    //radio button
        //}
        //#endregion

        #region "Aircraft Area"
        protected void btnEditAircraft_Click(object sender, EventArgs e)
{

    lblAircraftHrRate.Visible = false;
    lblAvgSpeed.Visible = false;
    lblReqRunway.Visible = false;
    lblEngine.Visible = false;
    lblSeatCount.Visible = false;
    lblTailNumber.Visible = false;

    txtAircraftHrRate.Visible = true;
    txtAvgSpeed.Visible = true;
    txtReqRunway.Visible = true;
    txtEngine.Visible = true;
    txtSeatCount.Visible = true;
    txtTailNumber.Visible = true;

    btnConfirmAircraft.Visible = true;
    btnEditAircraft.Visible = false;
    btnEditTaxes.Visible = false;
    btnEditFBO.Visible = false;
    btnEditOps.Visible = false;
}

protected void btnConfirmAircraft_Click(object sender, EventArgs e)
{
            var aircraftToDisplay = new RequestById();
            aircraftToDisplay.id = "55a74198e4b07e03cc6ac65a";
            var aircraftResponse = RestClientHelper.Post<Aircraft>("readPlane", aircraftToDisplay, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
            var WinnerAircraftObject = JsonConvert.DeserializeObject<Aircraft>(aircraftResponse.FunctionResponse.ToString());

            // var WinnerAircraftObject = JsonConvert.DeserializeObject<Aircraft>(Session["aircraft"].ToString());

            //var WinnerAircraftObject = Session["aircraft"];
            //Aircraft WinnerAircraftObject = new Aircraft();
            //var WinnerAircraftObject = Session["aircraft"].ToString();

            //       Aircraft WinnerAircraftObject = (Aircraft)Session["aircraft"];


            if (!String.IsNullOrEmpty(txtAircraftHrRate.Text))
    {
        WinnerAircraftObject.fees.hourlyRate = Convert.ToInt64(txtAircraftHrRate.Text);
        lblAircraftHrRate.Text = WinnerAircraftObject.fees.hourlyRate.ToString();
    }
    if (!String.IsNullOrEmpty(txtAvgSpeed.Text))
    {
        WinnerAircraftObject.speed = Convert.ToInt64(txtAvgSpeed.Text);
        lblAvgSpeed.Text = WinnerAircraftObject.speed.ToString();
    }
    if (!String.IsNullOrEmpty(txtReqRunway.Text))
    {
        WinnerAircraftObject.runwayLength = Convert.ToInt64(txtReqRunway.Text);
        lblReqRunway.Text = WinnerAircraftObject.runwayLength.ToString();
    }
    if (!String.IsNullOrEmpty(txtEngine.Text))
    {
        WinnerAircraftObject.engine = txtEngine.Text.ToString();
        lblEngine.Text = WinnerAircraftObject.engine.ToString();
    }
    if (!String.IsNullOrEmpty(txtSeatCount.Text))
    {
        WinnerAircraftObject.seatCount = Convert.ToInt32(txtSeatCount.Text);
        lblSeatCount.Text = WinnerAircraftObject.seatCount.ToString();
    }
    if (!String.IsNullOrEmpty(txtTailNumber.Text))
    {
        WinnerAircraftObject.tailNumber = txtTailNumber.Text.ToString();
        lblTailNumber.Text = WinnerAircraftObject.tailNumber.ToString();
    }

    var sendAircraftUpdate = new sendUpdate();
    sendAircraftUpdate.id = "55a74198e4b07e03cc6ac65a";
    sendAircraftUpdate.updates = WinnerAircraftObject;
    var updateResponse = RestClientHelper.Post<sendUpdate>("updatePlane", sendAircraftUpdate, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
    var isSucess = updateResponse.Success.ToString();

    txtAircraftHrRate.Visible = false;
    txtAvgSpeed.Visible = false;
    txtReqRunway.Visible = false;
    txtEngine.Visible = false;
    txtSeatCount.Visible = false;
    txtTailNumber.Visible = false;

    lblAircraftHrRate.Visible = true;
    lblAvgSpeed.Visible = true;
    lblReqRunway.Visible = true;
    lblEngine.Visible = true;
    lblSeatCount.Visible = true;
    lblTailNumber.Visible = true;

    btnEditAircraft.Visible = true;
    btnConfirmAircraft.Visible = false;
    btnEditTaxes.Visible = true;
    btnEditFBO.Visible = true;
    btnEditOps.Visible = true;


}
        #endregion

        #region "Tax Area"
        protected void btnEditTaxes_Click(object sender, EventArgs e)
        {
            txtFETTax.Visible = true;
            txtWRPA.Visible = true;
            txtOhioTax.Visible = true;
            lblFETTax.Visible = false;
            lblWRPA.Visible = false;
            lblOhioTax.Visible = false;

            btnConfirmTaxes.Visible = true;
            btnEditAircraft.Visible = false;
            btnEditTaxes.Visible = false;
            btnEditFBO.Visible = false;
            btnEditOps.Visible = false;
        }

        protected void btnConfirmTaxes_Click(object sender, EventArgs e)
        {
            taxWinner taxUpdate= (taxWinner)Session["taxes"];

            if (!String.IsNullOrEmpty(txtFETTax.Text))
            {
                taxUpdate.fetRate = Convert.ToDouble(txtFETTax.Text);
                lblFETTax.Text = taxUpdate.fetRate.ToString();
            }
            if (!String.IsNullOrEmpty(txtWRPA.Text))
            {
                taxUpdate.wrpaRate = Convert.ToDouble(txtWRPA.Text);
                lblWRPA.Text = taxUpdate.wrpaRate.ToString();
            }
            if (!String.IsNullOrEmpty(txtOhioTax.Text))
            {
                taxUpdate.state.OH = Convert.ToDouble(txtOhioTax.Text);
                lblOhioTax.Text = taxUpdate.state.OH.ToString();
            }

            var sendTaxUpdate = new sendUpdate();
            sendTaxUpdate.id = "56d139b7c1b0bac5e99f4831";
            sendTaxUpdate.updates = taxUpdate;
            var updateResponse = RestClientHelper.Post<sendUpdate>("updateTaxWinner", sendTaxUpdate, Session["LoggedInUserToken"].ToString(), Session["LoggedInUser"].ToString());
            var isSucess = updateResponse.Success.ToString();

            txtFETTax.Visible = false;
            txtWRPA.Visible = false;
            txtOhioTax.Visible = false;
            lblFETTax.Visible = true;
            lblWRPA.Visible = true;
            lblOhioTax.Visible = true;

            btnConfirmTaxes.Visible = false;
            btnEditAircraft.Visible = true;
            btnEditTaxes.Visible = true;
            btnEditFBO.Visible = true;
            btnEditOps.Visible = true;
        }
        #endregion

    }
}