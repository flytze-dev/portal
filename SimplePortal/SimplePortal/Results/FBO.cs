﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using SimplePortal.Objects;

//goal = make general FBO class to initialize FBOs when pulling info from Mongo
//the fields in FBOs.cs should directly match the mongo collection for FBO

namespace SimplePortal.Results
{
    public class FBO
    {
        [JsonProperty("agreement")]
        public string Agreement { get; set; }
        [JsonProperty("costPerHour")]
        public string CostPerHour { get; set; }
        [JsonProperty("dailyCharge")]
        public string DailyCharge { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        //isn't cost per hour an aircraft charge?
        [JsonProperty("fees")]
        public Fees fees { get; set; }
        [JsonProperty("flatCharge")]
        public object FlatCharge { get; set; }
        [JsonProperty("id")]
        public string FBOid { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }
    }







    //[JsonProperty("fbos")]
    ////public List<Objects.FBO> FBOList { get; set; }
    //public Objects.FBO[] FBOList { get; set; }

    //[JsonProperty("success")]
    //public bool Success { get; set; }

    //[JsonProperty("error")]
    //public string Error { get; set; }

    //[JsonProperty("errorDetails")]
    ////public string ErrorDetails { get; set; }
    //public List<object> ErrorDetails { get; set; }

    //[JsonProperty("errorMsg")]
    //public string ErrorMessage { get; set; }




    //[JsonProperty("phoneNumber")]
    //public string PhoneNumber { get; set; }

    //[JsonProperty("percentIncrease")]
    //public string PercentIncrease { get; set; }

    //[JsonProperty("name")]
    //public string Name { get; set; }

    //[JsonProperty("email")]
    //public string Email { get; set; }

    //[JsonProperty("flatCharge")]
    //public double FlatCharge { get; set; }

    //[JsonProperty("costPerHour")]
    //public double CostPerHour { get; set; }

    //[JsonProperty("dailyCharge")]
    //public double DailyCharge { get; set; }



    //[JsonProperty("increaseFrequency")]
    //public string IncreaseFrequency{ get; set; }

    //[JsonProperty("id")]
    //public string Id { get; set; }


    //public List <Name> AircraftList { get; set; }

    //do I need this initializer everytime?
    //public FBOs(string name)
    //{
    //    Name = name;
    //}
    //public FBO(string response)
    //    {
    //    }

    

    //public class OperationResult
    //{
    //    [JsonProperty("error")]
    //    public string Error { get; set; }

    //    [JsonProperty("success")]
    //    public bool Success { get; set; }
    //}
}