﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SimplePortal.Results
{
    public class Account
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("privilege")]
        public int Privilege { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("dateOfBirth")]
        public string DOB { get; set; }

        [JsonProperty("homeAirportId")]
        public string HomeAirport { get; set; }

        [JsonProperty("public")]
        public Public Public { get; set; }

        [JsonProperty("wallToken")]
        public string WallToken { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("type")]
        public string AccountType{ get; set; }

        [JsonProperty("stripeCustomerId")]
        public string StripeCustomerId { get; set; }

        [JsonProperty("tripHistory")]
        public TripHistory TripHistory { get; set; }

        [JsonProperty("accessGroup")]
        public TripHistory AccessGroup{ get; set; }
    }

    public class TripHistory
    {
        //how to make it so anything can be entered?
    }

    public class Public
    {
        //how to make it so anything can be entered?
    }

    public class LockOut
    {
        [JsonProperty("attempts")]
        public Public Attempts { get; set; }

        [JsonProperty("resetAt")]
        public TripHistory ResetAt { get; set; }
    }

}
