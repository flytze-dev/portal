﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json;


namespace SimplePortal.Results
{
    public class ExchangeRates
    {
        [JsonProperty("disclaimer")]
        public string Disclaimer { get; set; }
        [JsonProperty("license")]
        public string License { get; set; }
        public int TimeStamp { get; set; }
        public string Base { get; set; }
        public Dictionary<string, decimal> Rates { get; set; }


        //pass serialized Json into ExchangeRates object using WebCLient
        //app id: f446405a71124b28bf50515de13efcde
        //endpoint: https://openexchangerates.org/api/ 
        //example: /api/latest.json?app_id=f446405a71124b28bf50515de13efcde
        //source: https://openexchangerates.org/quick-start

        //public static ExchangeRates _download_serialized_json_data<ExchangeRates>(string url) where ExchangeRates : new()
        //{
        //    using (var w = new WebClient())
        //    {
        //        var json_data = string.Empty;
        //        // attempt to download JSON data as a string
        //        try
        //        {
        //            json_data = w.DownloadString(url);
        //        }
        //        catch (Exception) { }
        //        // if string with JSON data is not empty, deserialize it to class and return its instance 
        //        return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<ExchangeRates>(json_data) : new ExchangeRates();
        //    }
        //}
    }
}
