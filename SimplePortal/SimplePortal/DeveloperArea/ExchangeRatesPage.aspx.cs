﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Helpers;
using SimplePortal.Results;
using SimplePortal.Objects;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace SimplePortal.DeveloperArea
{
    public partial class ExchangeRatesPage : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            //var url = "https://openexchangerates.org/api/latest.json?app_id=f446405a71124b28bf50515de13efcde";
            //XX GridView1.DataSource = exchangeRates.Rates;     

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            lblSalt.Text = Security.Salt(20).ToString();
            var saltToUse = Security.Salt(20);
            var saltUserValue = Security.saltUser(saltToUse).ToString();
            lblsaltUser.Text = Security.encodeUTF8(saltUserValue).ToString();
            lblHash.Text = Security.createHash(saltUserValue).ToString();

            var exchangeRates = RestClientHelper.Get<ExchangeRates>("api/latest.json?app_id=f446405a71124b28bf50515de13efcde");

            //txtUrl.Text = "Disclaimer:" + exchangeRates.Disclaimer + Environment.NewLine + "Rates:" + Environment.NewLine + string.Format(@"Key : {0} <br /> Value : {1}", Key, Value)));
            txtUrl.Text = "Disclaimer:" + exchangeRates.Disclaimer + Environment.NewLine + "Rates:" + Environment.NewLine +
                string.Join(Environment.NewLine, exchangeRates.Rates.Select(c => string.Format("Key : {0} -- Value : {1}", c.Key, c.Value)).ToArray());
            //lblUrl.Text = RestClientHelper.FlytzeServiceUrl;
            lblDisclaimer.Text = exchangeRates.Disclaimer.ToString();
            lblBase.Text = exchangeRates.Base.ToString();
            lblBase0.Text = exchangeRates.Rates.ToString();
            lblBase1.Text = exchangeRates.TimeStamp.ToString();
            lblBase2.Text = exchangeRates.License.ToString();


            var newFBO1 = new FBO();
            var emptyObject = new Object();

            // wallTest1.name = 
            //createFbo
            //updateFbo
            //deleteFbo
            //trips
            //login

            //need to call this
            //var returnFBOFull = JsonConvert.DeserializeObject<FboResponse>(jsonStringFull);

//            RestClientHelper.Post<FBO>("readFbos", emptyObject, "7wtRM1RvwnPIMOK3yb8RmtpDQ", "system@flytze.com");
            //+Results.FBOs newFBOTest = RestClientHelper.Post<Results.FBOs>("readFbos", emptyObject);

            //then initiate object
            //+WallResponse<FBOs> results2 = new WallResponse<FBOs>();
            //+Results.FBOs results2 = new Results.FBOs();
            //-RestClientHelper.Post<FBOs> results2 = new RestClientHelper.Post<FBOs>();
            //why doesn't results2. suggest FBOs' properties like exchangeRates does after var exchangeRates = RestClientHelper.Get...

            //then use var... to initialize object
            //var results2 = new WallResponse<FBOs>();
            //-var results2 = Results.FBOs();
            //this is how exchangeRates is done with Get, what is different with POST?
            var results2 = RestClientHelper.Post<FBO[]>("readFbos", emptyObject, "7wtRM1RvwnPIMOK3yb8RmtpDQ", "system@flytze.com");
            //var results2 = RestClientHelper.Post<WallResponse<FBO>>("readFbos", emptyObject);
            
            if (results2.Success == true)
            //results2 always returns null
            {
                //function.Text = newFBOTest.ToString(); just killed
                //@params.Text = wallTest1.Response.ToString();
                //@params.Text = newFBO.PhoneNumber;
                pepper.Text = results2.ToString();
                //txtinfo.Text = newFBOTest.PhoneNumber.ToString();
                //txtinfo.Text = results2.WallResponseData.PhoneNumber.ToString();
                //sentTime.Text = results2.WallResponseData.Name.ToString();
                //token.Text = results2.WallResponseData.Email.ToString();
            }
            else
            {
                //txtinfo.Text = results2.FBOList.Length.ToString();
                //sentTime.Text = results2.ErrorDetails.ToString();
                ////token.Text = results2.ErrorMsg.ToString();
                //function.Text = results2.ErrorMessage.ToString();
                //token.Text = results2.Error.ToString();
            }

            //RestClientHelper.Post<Account> = ; //Post<Accounts>()
            //var wallTest2 = RestClientHelper.Post<wallTest2>("none", requestParams);

            //var wallTest3 = Objects.WallResponse<WallRequest>;

            //String jsonString = results.ToString();
            //JToken root = JObject.Parse(jsonString);
            //JToken user = root["user"];
            //FBOs deserializedUser = JsonConvert.DeserializeObject<FBOs>(user.ToString());
            // token.Text = FBOs.PhoneNumber.ToString();

            //username.Text = newFBOTest.ToString();
        }

    }
}